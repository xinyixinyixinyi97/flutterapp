import 'dart:convert';
import 'package:flutterapp/models/newsArticle_Model.dart';
import 'package:http/http.dart' as http;

class Articles {
  List<ArticleModel> art = [];

  Future<void> getArticles() async {
    String url =
        'http://newsapi.org/v2/top-headlines?country=my&category=health&apiKey=2d525075c72b425fbebfbb030e8112c0';

    //import data from api
    var response = await http.get(url);

    //decode the data get from api which is in json format
    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == "ok") {
      jsonData["articles"].forEach((element) {
        if (element['urlToImage'] != null && element['description'] != null) {
          if (element['description'].toString().contains('Hypertension ')) {
            print(element['description']);
          }

          //import data from API and match with local variable
          ArticleModel articleModel = ArticleModel(
              title: element['title'],
              description: element['description'],
              url: element['url'],
              urlToImage: element['urlToImage'],
              //publishedTime: element['publishedAt'],
              content: element['content']);

          art.add(articleModel);
        }
      });
    }
  }
}

// ignore: camel_case_types
class newsCategory {
  List<ArticleModel> art = [];

  Future<void> getArticles(String category) async {
    String url =
        'http://newsapi.org/v2/top-headlines?country=my&category=health&apiKey=2d525075c72b425fbebfbb030e8112c0';
    //import data from api
    var response = await http.get(url);

    //decode the data get from api which is in json format
    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == "ok") {
      jsonData["articles"].forEach((element) {
        if (element['urlToImage'] != null && element['description'] != null) {

          if (element['description'].toString().contains(category) ||
              element['title'].toString().contains(category)) {

            //import data from API and match with local variable
            ArticleModel articleModel = ArticleModel(
                title: element['title'],
                description: element['description'],
                url: element['url'],
                urlToImage: element['urlToImage'],
                //publishedTime: element['publishedAt'],
                content: element['content']);

            art.add(articleModel);
          }
        }
      });
    }
  }
}
