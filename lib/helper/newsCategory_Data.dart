import 'package:flutterapp/models/newsCategory_Model.dart';

List<CategoryModel> getCategory(){

  // ignore: non_constant_identifier_names
  List<CategoryModel> Category = new List<CategoryModel>();
  CategoryModel categoryModel;

  //1
  categoryModel = new CategoryModel();
  categoryModel.categoryName = 'COVID';
  categoryModel.categoryImage = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ1W7lmPkbaL6IORORKVEZgkBJ5J-Cw8fVRMn7BPWWygzaVHbTr&usqp=CAU";
  Category.add(categoryModel);


  //3
  categoryModel = new CategoryModel();
  categoryModel.categoryName = ' Hypertension ';
  categoryModel.categoryImage = 'https://www.health.harvard.edu/media/content/images/p8_BP_HL1802_ts537512310.jpg';
  Category.add(categoryModel);

  //4
  categoryModel = new CategoryModel();
  categoryModel.categoryName = ' diabetes';
  categoryModel.categoryImage ='https://res.cloudinary.com/grohealth/image/upload/c_fill,f_auto,fl_lossy,h_650,q_auto,w_1085/v1581695681/DCUK/Content/causes-of-diabetes.png';
  Category.add(categoryModel);

  //5
  categoryModel = new CategoryModel();
  categoryModel.categoryName = 'obesity';
  categoryModel.categoryImage = 'https://2rdnmg1qbg403gumla1v9i2h-wpengine.netdna-ssl.com/wp-content/uploads/sites/3/2018/11/obesityDiabCardioDisease-861999778-770x553-650x428.jpg';
  Category.add(categoryModel);

  return Category;
}