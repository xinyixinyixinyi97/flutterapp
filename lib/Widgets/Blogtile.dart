import 'package:flutter/material.dart';
import 'package:flutterapp/screens/newsArticle_views.dart';

class BlogTile extends StatelessWidget {
  final String imageURL, title, desc,url;

  BlogTile(
      {@required this.imageURL, @required this.title, @required this.desc,@required this.url});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>ArticleView(blockUrl: url)));
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: Image.network(imageURL)),
            SizedBox(height: 6.0),
            Text(title,style: TextStyle(fontSize: 20,color: Colors.black)),
            SizedBox(height: 6.0),
            Text(desc,style: TextStyle(fontSize: 20,color: Colors.black54)),
            SizedBox(height: 6.0),
          ],
        ),
      ),
    );
  }
}