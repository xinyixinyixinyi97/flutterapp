import 'package:flutter/material.dart';
import 'package:flutterapp/shared/NotificationPlugin.dart';

import 'NotificationPlugin.dart';

class LocalNotificationScreen extends StatefulWidget {
  @override
  _LocalNotificationScreenState createState() => _LocalNotificationScreenState();
}

class _LocalNotificationScreenState extends State<LocalNotificationScreen> {
  @override

  void initState(){
    super.initState();
    notificationPlugin.setListenerForLowerVersion(onNotificationInLowerVersions);
    notificationPlugin.setOnNotificationClick(onNotificationClick);
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Local noti"),
      ),
      body: Center(
        child: FlatButton(
          onPressed: () async {
            await notificationPlugin.scheduleNotification();
            // await notificationPlugin.showNotification();
          },
          child: Text("Send Notification"),
        ),
      ),
    );
  }
  onNotificationInLowerVersions(ReceivedNotification receivedNotification){

  }
  onNotificationClick(String payload){
    print('Payload $payload' );
  }
}
