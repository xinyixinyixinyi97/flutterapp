import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterapp/screens/Areminder.dart';
import 'package:flutterapp/screens/Medicine_Home.dart';
import 'package:flutterapp/screens/News.dart';
import 'package:flutterapp/screens/RemindernChecklist.dart';
import 'package:provider/provider.dart';
import 'package:flutterapp/screens/wrapper.dart';
import 'package:flutterapp/services/auth.dart';
import 'Pages/Food/search_screen.dart';
import 'Pages/MedicalReport/ViewBPTrend.dart';
import 'Pages/MedicalReport/medical_report.dart';
import 'Pages/MedicalReport/viewBGTrend.dart';
import 'Pages/MedicalReport/viewBMITrend.dart';
import 'models/user.dart';


final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

void main()  {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<User>(create: (context) => AuthService().user),

      ],
      child: MaterialApp(

        //initialRoute: LoginScreen.id,
        home: Wrapper(),

        routes: {
          // LoginScreen.id: (context) => LoginScreen(),
          '/wrapper': (context) => Wrapper(),
          News.id : (context) => News(),
          Areminder.id: (context) => Areminder(),
          RemindernChecklist.id: (context) => RemindernChecklist(),
          MedicineHome.id: (context) => MedicineHome(),

          '/view_med_report': (context) => MedicalReport(),
          '/Food_recipe': (context) => SearchScreen(),
          '/viewBMITrend':(context) => viewBMITrend(),
          '/viewBPTrend':(context) => viewBPTrend(),
          '/viewBGTrend':(context) => viewBGTrend(),
        },
      ),
    ); //MaterialApp
  }
}
