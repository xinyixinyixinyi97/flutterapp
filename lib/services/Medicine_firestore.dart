import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutterapp/models/MedicineAlarm_Model.dart';
import 'package:flutterapp/models/Medicine_Model.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class medicineFirestore extends ChangeNotifier {

  static final medicineFirestore _firestoreService = medicineFirestore._internal();
  Firestore _db = Firestore.instance;

  medicineFirestore._internal();

  factory medicineFirestore() {
    return _firestoreService;
  }

  Stream<List<Alarm>> getMedicineAlarmList(String med) {
    return _db.collection('MedAlarmList').where("medicineId", isEqualTo: med)
        .snapshots().map((snapshot) =>
    //convert snapshot to obj
    snapshot.documents
        .map(
          (doc) =>
        Alarm.fromMap(doc.data, doc.documentID),
    ).toList(),
    );
  }

  Future<void> addAlarm(Alarm alarm) {
    return _db.collection('MedAlarmList').add(alarm.toMap());
  }

  Stream<List<Medicine>> getMedicineList(String uid) {
    return _db
        .collection('medicinelist')
        .where("userID", isEqualTo: uid)
        .snapshots()
        .map(
          (snapshot) =>
              //convert snapshot to obj
              snapshot.documents
                  .map(
                    (doc) => Medicine.fromMap(doc.data, doc.documentID),
                  )
                  .toList(),
        );
  }

  //get the medicinelist to delete when user cancal half way
  Stream<List<Medicine>> getOneMedicine(String mid) {
    return _db
        .collection('medicinelist')
        .where("medID", isEqualTo: mid)
        .snapshots()
        .map(
          (snapshot) =>
      //convert snapshot to obj
      snapshot.documents
          .map(
            (doc) => Medicine.fromMap(doc.data, doc.documentID),
      )
          .toList(),
    );
  }

  Future<void> addMedicine(Medicine med) {
    return _db.collection('medicinelist').add(med.toMap());
  }

  Future<void> updateMedicine(Medicine med) {
    return _db
        .collection('medicinelist')
        .document(med.id)
        .updateData(med.toMap());
  }

  Future<void> deleteMedicine(String id) async {
    return _db.collection('medicinelist').document(id).delete();
  }

  Future<void> deleteAlarm(String id) async {
    return _db.collection('MedAlarmList').document(id).delete();
  }
}
