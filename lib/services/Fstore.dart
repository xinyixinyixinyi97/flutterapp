import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'file:///C:/Users/hoyux/StudioProjects/flutterapp/lib/models/Areminder.dart';
import 'package:intl/intl.dart';

class FirestoreService extends ChangeNotifier{

  static final FirestoreService _firestoreService = FirestoreService._internal();
  Firestore _db = Firestore.instance;

  FirestoreService._internal();

 factory FirestoreService(){
    return  _firestoreService;
  }

  //display in UI
  Stream<List<Appointment>> getAppointment(String uid) {

    //give stream of query snapshot
    return _db.collection('appointment').where("userID", isEqualTo: uid)
        .snapshots().map((snapshot) =>
    //convert snapshot to obj
    snapshot.documents.map((doc) =>
        Appointment.fromMap(doc.data, doc.documentID),
    ).toList(),
    );
  }

   Future<void> addAreminder(Appointment app){
     return _db.collection('appointment').add(app.toMap());
   }

  Future<void> updateAreminder(Appointment app) {
    return _db.collection('appointment').document(app.id).updateData(app.toMap());

  }

  Future<void> deleteAreminder(String id) {
    return _db.collection('appointment').document(id).delete();
  }



}