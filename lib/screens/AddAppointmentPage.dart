import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterapp/Widgets/Components.dart';
import 'file:///C:/Users/hoyux/StudioProjects/flutterapp/lib/models/Areminder.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/services/Fstore.dart';
import 'package:flutterapp/services/notificationID.dart';
import 'package:provider/provider.dart';
import 'package:add_2_calendar/add_2_calendar.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import '../Constants.dart';
import 'AppDetailsPage.dart';

class AddAppointmentPage extends StatefulWidget {
  final Appointment app;

  AddAppointmentPage({Key key, this.app}) : super(key: key);

  _AddAppointmentPageState createState() => _AddAppointmentPageState();
}

class _AddAppointmentPageState extends State<AddAppointmentPage> {

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  var initializationSettings;

  DateTime pickedDate;
  TimeOfDay pickedTime;

  GlobalKey<FormState> _key = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey;
  TextEditingController _nameController;
  TextEditingController _detailsController;
  TextEditingController _numberController;
  String dropdownValue;
  FocusNode _descriptionNode;
  String currentTimeZone;

  void initState() {

    super.initState();
    _scaffoldKey = GlobalKey<ScaffoldState>();

    _nameController =
        TextEditingController(text: isEditMote ? widget.app.APName : '');
    _detailsController =
        TextEditingController(text: isEditMote ? widget.app.APDetails : '');
    _numberController =
        TextEditingController(text: isEditMote ? widget.app.reminder : '');

    pickedTime = TimeOfDay.now();
    if (widget.app != null) {
      dropdownValue = widget.app.dhm;
      pickedDate = widget.app.APDate;
      pickedTime = TimeOfDay(
          hour: widget.app.APDate.hour, minute: widget.app.APDate.minute);
    } else {
      pickedDate = DateTime.now();
      pickedTime = TimeOfDay.now();
      dropdownValue = 'Minutes';
    }

    _descriptionNode = FocusNode();
    initializeNotifications();
    //_configureDidReceiveLocalNotificationSubject();
    //_configureSelectNotificationSubject();
  }

  get isEditMote => widget.app != null;

  Future<void> initializeNotifications() async {

    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    initializationSettings = InitializationSettings(android: initializationSettingsAndroid);

      await flutterLocalNotificationsPlugin.initialize(initializationSettings);


    setOnNotificationClick(Function onNotificationClick) async {
      await flutterLocalNotificationsPlugin.initialize(initializationSettings,
          onSelectNotification: (String payload) async {
            onNotificationClick(payload);
          });
    }
    onNotificationClick(String payload)
    {
      print(payload);
      Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => AppointmentDetailsPage(app: widget.app)));
    }

    tz.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation(currentTimeZone));
  }

//  Future onSelectNotification(String payload) async {
//    if (payload == null) {
//      print('notification payload: ' + payload);
//    }
//    await
//    Navigator.push(
//      context,
//      MaterialPageRoute(builder: (_) => AppointmentDetailsPage(app: widget.app)));
//  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    String passuserid = user.uid.toString();

    int a;

    //aff to local device calender
    Event event = Event(
      title: _nameController.text,
      description: _detailsController.text,
      location: 'Hospital K',
      startDate: pickedDate,
      endDate: pickedDate.add(Duration(days: 1)),
    );

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(isEditMote ? 'Edit Appointment' : 'Add Appointment'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(15.0),
        child: Form(
          key: _key,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
                textInputAction: TextInputAction.next,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(_descriptionNode);
                },
                controller: _nameController,
                validator: (value) {
                  if (value == null || value.isEmpty)
                    return "Appointment name cannot be empty";
                  return null;
                },
                decoration: InputDecoration(
                  labelText: "Appointment Name",
                  border: OutlineInputBorder(),
                ),
              ),
              const SizedBox(height: 10.0),
              TextFormField(
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
                textInputAction: TextInputAction.next,
                onEditingComplete: () {
                  FocusScope.of(context).requestFocus(_descriptionNode);
                },
                controller: _detailsController,
                maxLines: 4,
                decoration: InputDecoration(
                  labelText: "description",
                  border: OutlineInputBorder(),
                ),
              ),
              const SizedBox(height: 10.0),
              Column(
                children: <Widget>[
                  ListTile(
                    leading: Text('Appointment Date : ',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                    title: Text(
                      isEditMote ? getDate(pickedDate) : getDate(pickedDate),
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                      ),
                    ),
                    trailing: Icon(Icons.keyboard_arrow_down),
                    onTap: _pickDate,
                  ),
                  ListTile(
                    leading: Text('Appointment Time : ',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold)),
                    title: Text("${pickedTime.format(context)}",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        )),
                    trailing: Icon(Icons.keyboard_arrow_down),
                    onTap: _pickTime,
                  )
                ],
              ),
              const SizedBox(height: 10.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 200,
                    height: 50,
                    child: TextFormField(
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold),
                      controller: _numberController,
                      decoration: InputDecoration(
                        labelText: "Set Reminder",
                        border: OutlineInputBorder(),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.digitsOnly
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: DropdownButton<String>(
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                        value: dropdownValue,
                        onChanged: (String newValue) {
                          setState(() {
                            dropdownValue = newValue;
                          });
                        },
                        items: <String>['Day', 'Hour', 'Minutes']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                              value: value, child: Text(value));
                        }).toList()),
                  ),
                ],
              ),
              Container(
                width: 400,
                height: 50,
                child: Text('before the appointment',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20.0,
                    )),
              ),
              RoundedButton(
                title: isEditMote ? "Update" : "Save",
                colour: Color(0xFF3F51B5),
                onPressed: () async {

                  final snackBar =
                  SnackBar(content: Text("Please select the right time"));

                  if (_key.currentState.validate()) {

                    try {
                      DateTime datetime = DateTime(
                          pickedDate.year,
                          pickedDate.month,
                          pickedDate.day,
                          pickedTime.hour,
                          pickedTime.minute);

                      if(datetime.isBefore(DateTime.now()))
                        {
                          _scaffoldKey.currentState.showSnackBar(snackBar);
                        }
                      else
                        {
                          if (isEditMote) {
                            int notifid = widget.app.notificationID;
                            print(notifid);

                            Appointment app = Appointment(
                                APName: _nameController.text,
                                APDetails: _detailsController.text,
                                APDate: datetime,
                                id: widget.app.id,
                                userID: passuserid,
                                reminder: _numberController.text,
                                dhm: dropdownValue,
                                notificationID: notifid);

                            a = CalculateReminderDateTime(dropdownValue,
                                int.parse(_numberController.text), datetime);
                            print(a);
                            print(app.APDate);
                            print(app.id);
                            Navigator.pop(context);
                            await FirestoreService().updateAreminder(app);
                            showScheduledNotification(app, a);
                          } else {
                            int notifid = NotificationPlugin().createID();
                            print(notifid);

                            Appointment app = Appointment(
                                APName: _nameController.text,
                                APDetails: _detailsController.text,
                                APDate: datetime,
                                userID: passuserid,
                                reminder: _numberController.text,
                                dhm: dropdownValue,
                                notificationID: notifid);

                            Navigator.pop(context);
                            await FirestoreService().addAreminder(app);

                            print(app.userID);
                            print(app.APDate);

                            int a = CalculateReminderDateTime(dropdownValue,
                                int.parse(_numberController.text), app.APDate);
                            //print (a);
                            showScheduledNotification(app, a);
                          }
                        }

                    } catch (e) {
                      print(e);
                    }
                  }
                },
              ),
              RoundedButton(
                  title: 'Add to calander',
                  colour: Color(0xFF3F51B5),
                  onPressed: () async {
                    print(event.endDate);
                    Add2Calendar.addEvent2Cal(event).then((success) {
                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                          content: Text(success ? 'Success' : 'Error')));
                    });
                  })
            ],
          ),
        ),
      ),
    );
  }

  _pickDate() async {
    DateTime date = await showDatePicker(
      context: context,
      initialDate: pickedDate,
      firstDate: DateTime(DateTime.now().year),
      lastDate: DateTime(DateTime.now().year + 5),
    );

    if (date != null) {
      setState(() {
        pickedDate = date;
      });
    }
  }

  _pickTime() async {
    TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: pickedTime,
    );

    if (time != null) {
      setState(() {
        pickedTime = time;
        print(pickedTime);
      });
    }
  }

  Future<bool> showErrorDialog(BuildContext context) async {
    return await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              content: Text("Please enter a valid reminder time"),
              actions: <Widget>[
                FlatButton(
                  textColor: Colors.red,
                  child: Text("OKAY"),
                  onPressed: () => Navigator.pop(context, true),
                ),
              ],
            ));
  }

  void showScheduledNotification(Appointment app, int time) async {
    await _scheduledNotification(app, time);
    //await _showFullScreenNotification(app, time);
    //await _scheduleDailyTenAMNotification(app);
  }

  Future<void> _scheduledNotification(Appointment app, int duration) async {
    String name = app.APName;
    DateTime dateTime = app.APDate;
    print(duration);

    await flutterLocalNotificationsPlugin.zonedSchedule(
        app.notificationID,
        name,
        getDateandTime(dateTime).toString(),
        tz.TZDateTime.now(tz.local).add(Duration(seconds: duration)),
        const NotificationDetails(
            android: AndroidNotificationDetails(
                'your channel id',
                'your channel name',
                'your channel description',
              priority: Priority.high,
              importance: Importance.high,
              playSound: true,)),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime);
  }


  int CalculateReminderDateTime(
      String dropdownValue, int number, DateTime dateTime) {
    int second;
    int pickedTimeD = dateTime.day;
    int pickedTimeH = dateTime.hour;
    int pickedTimeS = dateTime.second;
    int pickedTimeM = dateTime.minute;

    DateTime now = DateTime.now();

    switch (dropdownValue) {
      case 'Day':
        {
          second = ((number * 24) * 60) * 60;
        }
        break;

      case "Hour":
        {
          second = (number * 60) * 60;
        }
        break;

      case "Minutes":
        {
          second = number * 60;
        }
        break;

      default:
        {
          print('print choose the types of time');
        }
        break;
    }
    int day = ((pickedTimeD - now.day) * 24) * 3600;
    int h = (pickedTimeH - now.hour) * 3600;
    int s = now.second - pickedTimeS;
    int m = (pickedTimeM - now.minute) * 60;

    //print (day);
    int d = ((h + m + day) - s) - second;

    return d;
  }
}
