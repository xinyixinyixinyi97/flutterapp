import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Widgets/Blogtile.dart';
import 'package:flutterapp/helper/newsArticle_Data.dart';
import 'package:flutterapp/helper/newsCategory_Data.dart';
import 'package:flutterapp/models/newsArticle_Model.dart';
import 'package:flutterapp/models/newsCategory_Model.dart';

import 'newsCategory_views.dart';

class News extends StatefulWidget {
  static const String id = 'News_screen';

  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  //show catogary on top
  List<CategoryModel> category = new List<CategoryModel>();

  //show articles
  List<ArticleModel> articles = new List<ArticleModel>();
  List<ArticleModel> _search = [];
  bool _loading = true;

  @override
  void initState() {
    // TODO: implement setState
    super.initState();
    //get object in catogary
    category = getCategory();
    //at the loading state, get articles
    getArticles();
  }

  getArticles() async {
    Articles artClass = new Articles();
    await artClass.getArticles();
    articles = artClass.art;
    setState(() {
      _loading = false;
    });
  }

  TextEditingController search = new TextEditingController();

  onSearch(String text) async {
    _search.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }
    articles.forEach((f) {
      if (f.title.contains(text) || f.description.contains(text)) {
        _search.add(f);
      }
    });

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[Text('News')],
        ),
      ),
      body: _loading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: <Widget>[
                    //Catogary
                    Container(
                      //padding: EdgeInsets.all(10.0),
                      color: Colors.black,
                      child: Card(
                        child: ListTile(
                          leading: Icon(Icons.search),
                          title: TextField(
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold),
                            controller: search,
                            onChanged: onSearch,
                            decoration: InputDecoration(
                                hintText: "Search", border: InputBorder.none),
                          ),
                          trailing: IconButton(
                            onPressed: () {
                              search.clear();
                              onSearch("");
                            },
                            icon: Icon(Icons.cancel),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 70.0,
                      padding: EdgeInsets.only(top: 10),
                      child: ListView.builder(
                          itemCount: category.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return CategoryTile(
                              image: category[index].categoryImage,
                              categoryName: category[index].categoryName,
                            );
                          }),
                    ),
                    //newsBlog
                    Container(
                      padding: EdgeInsets.only(top: 10),
                      child: _search.length != 0 || search.text.isNotEmpty
                          ? ListView.builder(
                              itemCount: _search.length,
                              itemBuilder: (context, index) {
                                final b = _search[index];
                                return BlogTile(
                                  imageURL: b.urlToImage,
                                  title: b.title,
                                  desc: b.description,
                                  url: b.url,
                                );
                              })
                          : ListView.builder(
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                              itemCount: articles.length,
                              itemBuilder: (context, index) {
                                return BlogTile(
                                  imageURL: articles[index].urlToImage,
                                  title: articles[index].title,
                                  desc: articles[index].description,
                                  url: articles[index].url,
                                );
                              }),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}

class CategoryTile extends StatelessWidget {
  final String image;
  final String categoryName;

  const CategoryTile({Key key, this.image, this.categoryName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CategoryView(category: categoryName)));
      },
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Stack(
          children: <Widget>[
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: Image.network(image,
                    height: 70.0, width: 120.0, fit: BoxFit.cover)),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Colors.black38),
              alignment: Alignment.center,
              height: 70.0,
              width: 120.0,
              child: Text(
                categoryName,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
