import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/models/MedicineAlarm_Model.dart';
import 'package:flutterapp/models/Medicine_Model.dart';
import 'package:flutterapp/screens/Medicine_NewEntry.dart';
import 'package:flutterapp/services/Medicine_firestore.dart';
import '../Constants.dart';

class medicineReminderDetails extends StatefulWidget {
  final Medicine medicine;

  const medicineReminderDetails({Key key, this.medicine}) : super(key: key);

  @override
  _medicineReminderDetailsState createState() =>
      _medicineReminderDetailsState();
}

class _medicineReminderDetailsState extends State<medicineReminderDetails> {
  String frequencyValue;
  bool _DayIntervalChecked;
  bool _SpecificDayChecked;

  void initState() {
    super.initState();
    frequencyValue = widget.medicine.frequency;

    if (frequencyValue == "Day Interval") {
      _DayIntervalChecked = true;
    } else {
      _DayIntervalChecked = false;
    }

    if (frequencyValue == "Specific Day") {
      _SpecificDayChecked = true;
    } else {
      _SpecificDayChecked = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Medicine Reminder Details'),
        backgroundColor: Color(0xFF3F51B5),
      ),
      body: SingleChildScrollView(
          padding: const EdgeInsets.all(10.0),
          child: Column(children: <Widget>[
            MedicineCard(widget.medicine),
            Text("Frequency : $frequencyValue",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 21.0,
                    fontWeight: FontWeight.bold)),
            SizedBox(height: 10),
            Visibility(
              visible: _DayIntervalChecked,
              child: Text("Every ${widget.medicine.intervalDay} Days",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    //fontWeight: FontWeight.bold
                  )),
            ),
            SizedBox(height: 10),
            Visibility(
              visible: _SpecificDayChecked,
              child: Text("Every  ${widget.medicine.specificDay} Days",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.0,
                    //fontWeight: FontWeight.bold
                  )),
            ),
            SizedBox(height: 10),
            Container(child: AlarmList(widget.medicine)),
            Row(children: [
              SizedBox(width: 250),
              IconButton(
                color: Color(0xFF3F51B5),
                icon: Icon(Icons.edit),
                iconSize: 50,
                onPressed: () {Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        MedicineNewEntry(med: widget.medicine),
                  ),
                );}
              ),
              IconButton(
                color: Color(0xFF3F51B5),
                icon: Icon(Icons.delete),
                iconSize: 50,
                onPressed: () => _deleteApp(context, widget.medicine),
              ),

            ],),
            //Text(getDate(widget.medicine.startDate)),
          ])),
    );
  }
}

void _deleteApp(BuildContext context, Medicine med) async {

  if (await _showConfirmationDialog(context)) {
    try {
      await medicineFirestore().deleteMedicine(med.id);
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }
}

Future<bool> _showConfirmationDialog(BuildContext context) async {
  return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) => AlertDialog(
        content: Text("Confirmation to delete",style: TextStyle(
            color: Colors.black,
            fontSize: 20.0,
            fontWeight: FontWeight.bold)),
        actions: <Widget>[
          FlatButton(
            child: Text("Delete",style: TextStyle(
                color: Colors.red,
                fontSize: 19.0,
                fontWeight: FontWeight.bold)),
            onPressed: () => Navigator.pop(context, true),
          ),
          FlatButton(
            textColor: Colors.black,
            child: Text("No",style: TextStyle(
                color: Colors.black,
                fontSize: 19.0,
                fontWeight: FontWeight.bold)),
            onPressed: () => Navigator.pop(context, false),
          ),
        ],
      ));
}

class AlarmList extends StatelessWidget {
  final Medicine med;

  AlarmList(this.med);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: medicineFirestore().getMedicineAlarmList(med.medID),
        builder: (BuildContext context, AsyncSnapshot<List<Alarm>> snapshot) {
          if (snapshot.hasError)
            return Container(
                child: Text('An error has occurred: ${snapshot.error}'));
          else if(snapshot.hasError)
            return Center(child: CircularProgressIndicator());
          else
          return Container(
              height: 300,
              width: 500,
              child: ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    Alarm alarm = snapshot.data[index];
                    int num = index+1;
                    return ListTile(
                      leading: Icon(const IconData(0xe801, fontFamily: "Icons"),
                          color: Color(0xFF3F51B5)),
                      title: Text(getTime(alarm.alarm),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 21.0,
                          )),
                      subtitle: Text(
                          "${alarm.dosage.toString()} ${returnDosage(med.medicineType)} ",
                          style: TextStyle(
                            color: Colors.blueGrey,
                            fontSize: 20.0,
                          )),
                      trailing: Text(num.toString(),
                          style: TextStyle(
                              fontSize: 22.0,
                              fontWeight: FontWeight.w900,
                              color: Colors.grey)),
                    );
                  }));
        });
  }
}

class MedicineCard extends StatelessWidget {
  final Medicine medicine;

  MedicineCard(this.medicine);

  Hero makeIcon(double size) {
    if (medicine.medicineType == "MedicineType.Bottle") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe900, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    } else if (medicine.medicineType == "MedicineType.Pill") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe901, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    } else if (medicine.medicineType == "MedicineType.Syringe") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe902, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    } else if (medicine.medicineType == "MedicineType.Tablet") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe903, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    }
    return Hero(
      tag: medicine.medicineName + medicine.medicineType,
      child: Icon(
        Icons.error,
        color: Color(0xFF3F51B5),
        size: size,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10.0),
      child: InkWell(
        highlightColor: Colors.white,
        splashColor: Colors.grey,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white10,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                makeIcon(80.0),
                Hero(
                  tag: medicine.medicineName,
                  child: Material(
                    color: Colors.transparent,
                    child: Text(
                      medicine.medicineName,
                      style: TextStyle(
                          fontSize: 22,
                          color: Color(0xFF3F51B5),
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

returnDosage(String medType) {
  switch (medType) {
    case "MedicineType.Pill":
      {
        return "Dosage in Pill";
      }
      break;
    case "MedicineType.Bottle":
      {
        return "Dosage in mL";
      }
      break;
    case "MedicineType.Syringe":
      {
        return "Dosage in Injection";
      }
      break;
    case "MedicineType.Tablet":
      {
        return "Dosage in Tablet";
      }
      break;
  }
}
