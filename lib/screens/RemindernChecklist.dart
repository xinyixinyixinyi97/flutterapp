import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/screens/Areminder.dart';
import 'package:flutterapp/screens/Checklist.dart';
import 'package:flutterapp/screens/Medicine_Home.dart';
import 'package:flutterapp/screens/News.dart';
import 'package:flutterapp/services/auth.dart';
import 'package:provider/provider.dart';

// import 'EmergencyCall.dart';


class RemindernChecklist extends StatefulWidget {
  static const id = 'RemindernChecklist_screen';

  @override
  _RemindernChecklist createState() => _RemindernChecklist();
}

class _RemindernChecklist extends State<RemindernChecklist> {

  final AuthService _auth = AuthService();
  FlutterLocalNotificationsPlugin localNotification;

  @override
  void initState(){
    super.initState();
    var androidInitialize = new AndroidInitializationSettings('app_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(android: androidInitialize,iOS:iOSInitialize );
    localNotification = new FlutterLocalNotificationsPlugin();
    localNotification.initialize(initializationSettings);
  }

  Future _showNotification()async{
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotification.show(0,"Health Records Reminder", "Have you take your record today?", generalNotificationDetails);

  }
  Future _showScheduleNotification1()async{
    var time = Time(0,37,0);
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotification..showDailyAtTime(0,"Daily fffRecords Reminder", "Have you take your record today?",time ,generalNotificationDetails);
  }
  Future _showScheduleNotification()async{
    var scheduleNotificationDateTime = DateTime.now().add(Duration(seconds: 5));
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotification.schedule(0,"Health Records Reminder", "Have you take your record today?",scheduleNotificationDateTime ,generalNotificationDetails);

  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    String passuserid = user.uid.toString();

    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        centerTitle: true,
        title: Text('Vcare',
            style: TextStyle(
              fontSize: 25,
            )),
        backgroundColor: Colors.lightBlue[900],
//        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
              onPressed: () async {
                await _auth.signOut();
              },
            // onPressed: (){_showNotification();},
              child: Row(
                children: [

                  Text(
                    'Logout ',
                    style: TextStyle(fontSize: 20, color: Colors.white),
                  ),
                  Icon(
                    Icons.login_outlined,
                    color: Colors.white,
                  ),
                ],
              )),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                GestureDetector(
//                splashColor: Colors.amber,
                  onTap: () {
                    Navigator.pushNamed(context, '/view_med_report');
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25.0),
                      gradient: new LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromARGB(255, 0, 146, 255),
                          Color.fromARGB(255, 27, 187, 255)
                        ],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 2,
                          offset: Offset(1, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    height: 150,
                    child: Column(
                      children: <Widget>[
                        Expanded(
                            child: Icon(
                              Icons.assignment,
                              size: 60,
                              color: Colors.white,
                            )),
                        Text(
                          'Medical Report',
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.w900,
                              color: Colors.white),
                        ),
                        SizedBox(
                          height: 20.0,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/Food_recipe');
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.local_dining,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Food recipe',
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 3,),

                    Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
//                splashColor: Colors.amber,
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Home(userId:passuserid),
                            ),);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.format_list_numbered_rounded,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Checklist',
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                height: 20.0,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
//                splashColor: Colors.amber,
                        onTap: () {
                          Navigator.pushNamed(context, Areminder.id);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.date_range_outlined,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Appointment',
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                height: 20.0,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, MedicineHome.id);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.remove_circle,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Medicine',
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/NearbyHospitalScreen');
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.local_hospital_rounded,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Hospital',
                                style: TextStyle(
                                    fontSize: 25,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w900),
                              ),
                              SizedBox(
                                height: 20.0,
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 3,),

                    Container(
                      width: MediaQuery.of(context).size.width/2.25,
                      child: GestureDetector(
//                splashColor: Colors.amber,
                        onTap: () {
                          // Navigator.pushNamed(context, EmergencyCallnSMS.id);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25.0),
                            gradient: new LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              colors: [
                                Color.fromARGB(255, 0, 146, 255),
                                Color.fromARGB(255, 27, 187, 255)
                              ],
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.5),
                                spreadRadius: 1,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ],
                          ),
                          height: 150,
                          child: Column(
                            children: <Widget>[
                              Expanded(
                                  child: Icon(
                                    Icons.call,
                                    size: 60,
                                    color: Colors.white,
                                  )),
                              Text(
                                'Call & SMS',
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                height: 20.0,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10,),
                Container(
                  width: MediaQuery.of(context).size.width/2.7,
                  child: GestureDetector(
//                splashColor: Colors.amber,
                    onTap: () {
                      Navigator.pushNamed(context, News.id);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25.0),
                        gradient: new LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          colors: [
                            Color.fromARGB(255, 0, 146, 255),
                            Color.fromARGB(255, 27, 187, 255)
                          ],
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 2,
                            offset: Offset(1, 3), // changes position of shadow
                          ),
                        ],
                      ),
                      height: 150,
                      child: Column(
                        children: <Widget>[
                          Expanded(
                              child: Icon(
                                Icons.public,
                                size: 60,
                                color: Colors.white,
                              )),
                          Text(
                            'News',
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w900,
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 20.0,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
