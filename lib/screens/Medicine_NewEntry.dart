import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/Widgets/Components.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/screens/Medicine_Reminder.dart';
import 'package:flutterapp/services/Medicine_firestore.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutterapp/models/Medicine_Model.dart';
import 'package:flutterapp/models/Medicine_type.dart';
import 'package:uuid/uuid.dart';

class MedicineNewEntry extends StatefulWidget {
  final Medicine med;

  const MedicineNewEntry({Key key, this.med}) : super(key: key);

  @override
  _MedicineNewEntryState createState() => _MedicineNewEntryState();
}

class MTWTFSS {
  String mtwtfss;
  bool daySelection;

  MTWTFSS(this.mtwtfss, this.daySelection);

  @override
  String toString() {
    return ' $mtwtfss';
  }
}

class _MedicineNewEntryState extends State<MedicineNewEntry> {
  TextEditingController nameController;
  GlobalKey<ScaffoldState> _scaffoldKey;
  GlobalKey<FormState> _key = GlobalKey<FormState>();
  NewEntryBloc _newEntryBloc;
  String frequencyValue;
  List<String> selecteditems = List();
  Map<String, bool> MTWTFSSvalues = {
    'Sunday': false,
    'Monday': false,
    'Tuesday': false,
    'Wednesday': false,
    'Thursday': false,
    'Friday': false,
    'Saturday': false,
  };
  int intervalDay;
  String medtype;
  int alarmValue;

  void initState() {
    super.initState();

    //print(widget.med.toMap());
    nameController =
        TextEditingController(text: isEditMode ? widget.med.medicineName : '');
    frequencyValue = isEditMode ? widget.med.frequency : "Everyday";
    medtype = isEditMode ? widget.med.medicineType : "";
    intervalDay = isEditMode ? widget.med.intervalDay : null;
    _newEntryBloc = NewEntryBloc();
    _scaffoldKey = GlobalKey<ScaffoldState>();

    if (isEditMode) {

      alarmValue = widget.med.alarmValue;

      if (medtype == MedicineType.Tablet.toString())
        _newEntryBloc.updateSelectedMedicine(MedicineType.Tablet);
      else if (medtype == MedicineType.Bottle.toString())
        _newEntryBloc.updateSelectedMedicine(MedicineType.Bottle);
      else if (medtype == MedicineType.Syringe.toString())
        _newEntryBloc.updateSelectedMedicine(MedicineType.Syringe);
      else if (medtype == MedicineType.Pill.toString())
        _newEntryBloc.updateSelectedMedicine(MedicineType.Pill);
    }
  }

  get isEditMode => widget.med != null;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    String passuserid = user.uid.toString();

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Color(0xFF3F51B5),
        ),
        centerTitle: true,
        title: Text(
          "Add New MediReminder",
          style: TextStyle(
            color: Colors.black,
            fontSize: 25,
          ),
        ),
        elevation: 0.0,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 25,
        ),
        child: Provider<NewEntryBloc>.value(
          value: _newEntryBloc,
          child: Form(
            key: _key,
            child: ListView(children: <Widget>[
              PanelTitle(
                title: "Medicine Name",
                isRequired: true,
              ),
              TextFormField(
                maxLength: 20,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                ),
                controller: nameController,
                validator: (value) {
                  if (value == null || value.isEmpty)
                    return "Medicine name cannot be empty";
                  return null;
                },
                textCapitalization: TextCapitalization.words,
                decoration: InputDecoration(
                  border: UnderlineInputBorder(),
                ),
              ),
              PanelTitle(
                title: "Medicine Type",
                isRequired: true,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: StreamBuilder<MedicineType>(
                  stream: _newEntryBloc.selectedMedicineType,
                  builder: (context, snapshot) {
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        MedicineTypeColumn(
                            type: MedicineType.Bottle,
                            name: "Bottle",
                            iconValue: 0xe900,
                            isSelected: snapshot.data == MedicineType.Bottle
                                ? true
                                : false),
                        MedicineTypeColumn(
                            type: MedicineType.Pill,
                            name: "Pill",
                            iconValue: 0xe901,
                            isSelected: snapshot.data == MedicineType.Pill
                                ? true
                                : false),
                        MedicineTypeColumn(
                            type: MedicineType.Syringe,
                            name: "Syringe",
                            iconValue: 0xe902,
                            isSelected: snapshot.data == MedicineType.Syringe
                                ? true
                                : false),
                        MedicineTypeColumn(
                            type: MedicineType.Tablet,
                            name: "Tablet",
                            iconValue: 0xe903,
                            isSelected: snapshot.data == MedicineType.Tablet
                                ? true
                                : false),
                        MedicineTypeColumn(
                            type: MedicineType.Other,
                            name: "Other",
                            iconValue: 0xe904,
                            isSelected: snapshot.data == MedicineType.Other
                                ? true
                                : false),
                      ],
                    );
                  },
                ),
              ),
              const SizedBox(height: 20.0),
              PanelTitle(
                title: "Frequency",
                isRequired: true,
              ),
              DropdownButton(
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                  value: frequencyValue,
                  items: <String>['Everyday', 'Day Interval', 'Specific Day']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                        value: value, child: Text(value));
                  }).toList(),
                  onChanged: (String newFrequencyValue) {
                    setState(() {
                      frequencyValue = newFrequencyValue;
                    });
                  }),
              showFrequencyOption(frequencyValue),
              RoundedButton(
                  title: 'Next',
                  colour: Color(0xFF3F51B5),
                  onPressed: () async {
                    String medName;
                    String medType;
                    final snackBar =
                        SnackBar(content: Text("Please select Medicine Type"));

                    if (_key.currentState.validate()) {
                      if (_newEntryBloc.selectedMedicineType.value !=
                          MedicineType.None) {
                        if (frequencyValue == "Day Interval" &&
                            intervalDay == null)
                        {
                          print(intervalDay);
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text("Please select How Often")));
                        }
                        else if (frequencyValue == "Specific Day" && selecteditems.isEmpty)
                        {
                          print(selecteditems);
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text("Please select Which days")));
                        }
                        else {

                          if(isEditMode)
                            {
                              print("hello");
                              print(widget.med.alarmValue);
                              medName = nameController.text;
                              medType = _newEntryBloc.selectedMedicineType.value.toString();
                              //var medID = Uuid();

                              Medicine med = Medicine(
                                  medicineName: medName,
                                  medicineType: medType,
                                  medID: widget.med.medID,
                                  userID: passuserid,
                                  frequency: frequencyValue,
                                  intervalDay: intervalDay,
                                  specificDay: selecteditems,
                                  alarmValue: alarmValue,
                                  id:widget.med.id);


                              //print(widget.med.id);
                              //print(med.id);
                              print(med.medicineType);
                              print(med.alarmValue);
                              await medicineFirestore().updateMedicine(med);

                            //bring to new page
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      MedicineReminder(medicine: med),
                                ),
                              );
                            }
                          //add new medicine reminder
                          else
                          {
                            medName = nameController.text;
                            medType = _newEntryBloc.selectedMedicineType.value.toString();
                            var medID = Uuid();

                            Medicine med = Medicine(
                                medicineName: medName,
                                medicineType: medType,
                                medID: medID.v4(),
                                userID: passuserid,
                                frequency: frequencyValue,
                                intervalDay: intervalDay,
                                specificDay: selecteditems);

                            medicineFirestore().addMedicine(med);

                            //bring to new page
                            Navigator.pop(context);
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    MedicineReminder(medicine: med),
                              ),
                            );
                          }
                        }
                      } else {
                        _scaffoldKey.currentState.showSnackBar(snackBar);
                      }
                    }
                  }),
            ]),
          ),
        ),
      ),
    );
  }

  showFrequencyOption(String frequencyValue) {
    if (frequencyValue == 'Day Interval') {
      return Visibility(
        visible: true,
        child: Column(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: PanelTitle(
                title: "How Often?",
                isRequired: true,
              ),
            ),
            const SizedBox(height: 9.0),
            Row(
              children: <Widget>[
                Container(
                  width: 60,
                  height: 40,
                  child: Text(
                    'Every',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Container(
                  child: DropdownButton(
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                      value: intervalDay,
                      items: <int>[
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15,
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                      ].map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                            value: value, child: Text("$value"));
                      }).toList(),
                      onChanged: (int newintervalDay) {
                        setState(() {
                          intervalDay = newintervalDay;
                        });
                      }),
                ),
                const SizedBox(width: 10.0),
                Container(
                  width: 60,
                  height: 40,
                  child: Text(
                    'Day',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    } else if (frequencyValue == "Everyday") {
      return Visibility(visible: false, child: Text('haha'));
    } else if (frequencyValue == "Specific Day") {
      return Visibility(
        visible: true,
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            ExpansionTile(
              title: Text.rich(
                TextSpan(children: <TextSpan>[
                  TextSpan(
                    text: "Which days?",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  TextSpan(
                    text: " *",
                    style: TextStyle(fontSize: 20, color: Color(0xFF3F51B5)),
                  ),
                ]),
              ),
              children: <Widget>[
                Container(
                  height: 400,
                  child: ListView(
                    children: MTWTFSSvalues.keys.map((String key) {
                      return CheckboxListTile(
                        title: Text(key,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                            )),
                        value: MTWTFSSvalues[key],
                        onChanged: (bool value) {
                          setState(() {
                            MTWTFSSvalues[key] = value;
                            selecteditems.clear();
                            print('update the MTWTFSS List...');
                            MTWTFSSvalues.forEach((key, value) {
                              //print('${key}: ${value}');
                              if (value == true) {
                                selecteditems.add(key);
                                print(selecteditems);
                              }
                            });
                          });
                        },
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}

class MedicineTypeColumn extends StatelessWidget {
  final MedicineType type;
  final String name;
  final int iconValue;
  final bool isSelected;

  const MedicineTypeColumn(
      {Key key, this.type, this.name, this.iconValue, this.isSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NewEntryBloc _newEntryBloc = Provider.of<NewEntryBloc>(context);

    return GestureDetector(
      onTap: () {
        _newEntryBloc.updateSelectedMedicine(type);
      },
      child: Column(
        children: <Widget>[
          Container(
            width: 55,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: isSelected ? Color(0xFF3F51B5) : Colors.white,
            ),
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 10.0),
                child: Icon(
                  IconData(iconValue, fontFamily: "Ic"),
                  size: 65,
                  color: isSelected ? Colors.white : Color(0xFF3F51B5),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Container(
              width: 65,
              height: 30,
              decoration: BoxDecoration(
                color: isSelected ? Color(0xFF3F51B5) : Colors.transparent,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Center(
                child: Text(
                  name,
                  style: TextStyle(
                    fontSize: 18,
                    color: isSelected ? Colors.white : Color(0xFF3F51B5),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class NewEntryBloc {
  BehaviorSubject<MedicineType> _selectedMedicineType$;

  BehaviorSubject<MedicineType> get selectedMedicineType =>
      _selectedMedicineType$.stream;

  NewEntryBloc() {
    _selectedMedicineType$ =
        BehaviorSubject<MedicineType>.seeded(MedicineType.None);
  }

  void dispose() {
    _selectedMedicineType$.close();
  }

  void updateSelectedMedicine(MedicineType type) {
    MedicineType _tempType = _selectedMedicineType$.value;
    if (type == _tempType) {
      _selectedMedicineType$.add(MedicineType.None);
    } else {
      _selectedMedicineType$.add(type);
    }
  }
}
