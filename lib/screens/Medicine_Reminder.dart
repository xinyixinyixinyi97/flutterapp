import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutterapp/Widgets/Components.dart';
import 'package:flutterapp/models/MedicineAlarm_Model.dart';
import 'package:flutterapp/models/Medicine_Model.dart';
import 'package:flutterapp/services/Medicine_firestore.dart';
import 'package:flutterapp/services/notificationID.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class MedicineReminder extends StatefulWidget {
  final Medicine medicine;

  const MedicineReminder({Key key, this.medicine}) : super(key: key);

  @override
  _MedicineReminderState createState() => _MedicineReminderState();
}

class _MedicineReminderState extends State<MedicineReminder> {
  GlobalKey<ScaffoldState> _scaffoldKey;
  int dosagenum;
  int DayValue;
  bool isdayValueSelected;
  List<TimeOfDay> pickedTime;
  List<int> dosage;
  List<int> notificationIDs;
  List<String> ID;

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin=FlutterLocalNotificationsPlugin();
  var initializationSettings;
  String currentTimeZone;

  void initState() {
    super.initState();
    initializeNotifications();
    dosagenum = 1;
    _scaffoldKey = GlobalKey<ScaffoldState>();

    if(isEditMode)
      {
        DayValue = widget.medicine.alarmValue;
        print(widget.medicine.alarmValue);
      }

    isdayValueSelected = (isEditMode) ? true : false;
    pickedTime = List();
    dosage = List();
    notificationIDs = List();
    ID = List();
  }

  get isEditMode => widget.medicine.alarmValue != null;


  Future<void> initializeNotifications() async {

    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');

    initializationSettings = InitializationSettings(android: initializationSettingsAndroid);

    await flutterLocalNotificationsPlugin.initialize(initializationSettings);

    tz.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation(currentTimeZone));
  }

  @override
  Widget build(BuildContext context) {

    if(isEditMode)
    {
      return StreamBuilder<List<Alarm>>
        (
          stream: medicineFirestore().getMedicineAlarmList(widget.medicine.medID),
          builder: (BuildContext context, AsyncSnapshot<List<Alarm>> snapshot)
          {

          int l = snapshot.data.length;
          Alarm alarm;

          //ID.clear();
          print("ha$ID");

          if(pickedTime.isEmpty)
            {
              for(int i=0;i<l;i++)
              {
                alarm = snapshot.data[i];
                TimeOfDay t = TimeOfDay(hour:alarm.alarm.hour,minute:alarm.alarm.minute);
                pickedTime.add(t);
                notificationIDs.add(alarm.notificationID);
                dosage.add(alarm.dosage);
                ID.add(alarm.alarmID);
              }

              print("haha$ID");
              print("haha$pickedTime");
            }

              //print(notificationIDs);
              //print(dosage);

      return Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,

        appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Color(0xFF3F51B5),
        ),
        centerTitle: true,
        title: Text(
          "Add New MediReminder",
          style: TextStyle(
            color: Colors.black,
            fontSize: 25,
          ),
        ),
        elevation: 0.0,
      ),

        body: ListView(
          padding: EdgeInsets.symmetric(
            horizontal: 25,
          ),
          //child: Form(
          //child: ListView(
          children: <Widget>[
            PanelTitle(
              title: "HOW MANY TIMES A DAY?",
              isRequired: true,
            ),
            Row(
              children: <Widget>[
                SizedBox(width: 15),
                DropdownButton(
                    style: TextStyle(
                      fontSize: 19,
                      color: Colors.black,
                    ),
                    value: DayValue,
                    items: <int>[1, 2, 3, 4, 5, 6]
                        .map<DropdownMenuItem<int>>((int value) {
                      return DropdownMenuItem<int>(
                        value: value,
                        child: _getText(value),
                      );
                    }).toList(),
                    onChanged: (int newDayValue) {
                      setState(()  {
                        DayValue = newDayValue;
                        isdayValueSelected = true;

                        //1.delete previous data in firebase

                        if(snapshot.data.isNotEmpty)
                          {
                            for(int i=0;i<ID.length;i++)
                            {
                               medicineFirestore().deleteAlarm(ID[i]);
                               _cancelNotification(notificationIDs[i]);
                            }
                          }

                        //2.delete data in the List

                        ID.clear();
                        pickedTime.clear();
                        dosage.clear();
                        notificationIDs.clear();

                        //3.delete notifications



                          for (int i = 0; i < DayValue; i++) {
                            pickedTime.add(TimeOfDay.now());
                            dosage.add(1);
                            notificationIDs.add(NotificationPlugin().createID());

                          print(pickedTime);
                          print(dosage);
                          print(notificationIDs);
                          print(ID);

                          }
                        }
                      );
                    }),
              ],
            ),
//            PanelTitle(
//              title: "Start Date",
//              isRequired: false,
//            ),
//            ListTile(
//              title: Text(
//                getDate(startDate),
//                style: TextStyle(
//                    fontSize: 19.0,
//                    fontWeight: FontWeight.w500,
//                    color: Color(0xFF3F51B5)),
//              ),
//              onTap: _pickstartDate,
//            ),
//            PanelTitle(
//              title: "End Date",
//              isRequired: false,
//            ),
//            ListTile(
//              title: Text(
//                getDate(endDate),
//                style: TextStyle(
//                    fontSize: 19.0,
//                    fontWeight: FontWeight.w500,
//                    color: Color(0xFF3F51B5)),
//              ),
//              onTap: _pickendDate,
//            ),

            Container(
              height: 40,
              width: 100,
              child: Visibility(
                visible: isdayValueSelected,
                child: PanelTitle(
                  title: "SET TIME AND DOSAGE",
                  isRequired: false,
                ),
              ),
            ),
            Container(
                height: 400,
                width: 100,
                child: Visibility(
                    visible: true,
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        int num = index + 1;
                        return ListTile(
                          leading: Icon(
                              const IconData(0xe801, fontFamily: "Icons"),
                              color: Color(0xFF3F51B5)),
                          title: Text("${pickedTime[index].format(context)}",
                              style: TextStyle(fontSize: 19.0)),
                          subtitle: Text(
                              "${dosage[index]} ${returnDosage(widget.medicine.medicineType)}",
                              style: TextStyle(fontSize: 18.0)),
                          trailing: Text(num.toString(),
                              style: TextStyle(
                                  fontSize: 22.0,
                                  fontWeight: FontWeight.w900,
                                  color: Colors.grey)),
                          onTap: () {

                              for(int i=0;i<ID.length;i++)
                              {
                                medicineFirestore().deleteAlarm(ID[i]);
                              }

                            setState(() {
                              showAddTaskBottomSheet(context, index);
                            });
                          },
                        );
                      },
                      itemCount: DayValue,
                    ))),
            StreamBuilder(
                stream: medicineFirestore().getOneMedicine(widget.medicine.medID),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Medicine>> snapshot) {
                  return RoundedButton(
                      title: "Update",
                      colour: Color(0xFF3F51B5),
                      onPressed: () async {

                        int size = pickedTime.length;
                        String id = snapshot.data[0].id;

                          int a = 0;
                          print("update");
                          print(pickedTime);
                          print(id);


                          Medicine m = new Medicine(
                              medicineName: widget.medicine.medicineName,
                              medicineType: widget.medicine.medicineType,
                              medID: widget.medicine.medID,
                              userID: widget.medicine.userID,
                              frequency: widget.medicine.frequency,
                              id:id,
                              intervalDay:widget.medicine.intervalDay,
                              specificDay:widget.medicine.specificDay,
                              alarmValue: size
                          );

                        while (a < size) {
                          DateTime datetime = DateTime(
                              DateTime.now().year,
                              DateTime.now().month,
                              DateTime.now().day,
                              pickedTime[a].hour,
                              pickedTime[a].minute);

                          Alarm alarm = Alarm(
                              dosage: dosage[a],
                              notificationID: notificationIDs[a],
                              medicineId: widget.medicine.medID,
                              alarm: datetime);

                          medicineFirestore().addAlarm(alarm);
                          a++;

                            showScheduledNotification(m,alarm);
                            await _checkPendingNotificationRequests();
                        }

                          Navigator.pop(context);
                          await medicineFirestore().updateMedicine(m);

                          //bring to new page

                        }
                      );

                }),
          ],
        ),
        // ),
        //),
      );
          });
    }
    else
      {
        return Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,

          appBar: AppBar(
            backgroundColor: Colors.white,
            leading: StreamBuilder(
                stream: medicineFirestore().getOneMedicine(widget.medicine.medID),
                builder:
                    (BuildContext context, AsyncSnapshot<List<Medicine>> snapshot) {
                  return IconButton(
                      icon: new Icon(Icons.arrow_back, color: Color(0xFF3F51B5)),
                      onPressed: () async {
                        print(snapshot.data[0].id);
                        await medicineFirestore()
                            .deleteMedicine(snapshot.data[0].id);
                        Navigator.of(context).pop();
                      });
                }),
            centerTitle: true,
            title: Text(
              "Add New MediReminder",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
              ),
            ),
            elevation: 0.0,
          ),

          body: ListView(
            padding: EdgeInsets.symmetric(
              horizontal: 25,
            ),
            //child: Form(
            //child: ListView(
            children: <Widget>[
              PanelTitle(
                title: "HOW MANY TIMES A DAY?",
                isRequired: true,
              ),
              Row(
                children: <Widget>[
                  SizedBox(width: 15),
                  DropdownButton(
                      style: TextStyle(
                        fontSize: 19,
                        color: Colors.black,
                      ),
                      value: DayValue,
                      items: <int>[1, 2, 3, 4, 5, 6]
                          .map<DropdownMenuItem<int>>((int value) {
                        return DropdownMenuItem<int>(
                          value: value,
                          child: _getText(value),
                        );
                      }).toList(),
                      onChanged: (int newDayValue) {
                        setState(() {
                          DayValue = newDayValue;
                          isdayValueSelected = true;
                          if (pickedTime.isEmpty) {
                            for (int i = 0; i < DayValue; i++) {
                              pickedTime.add(TimeOfDay.now());
                              dosage.add(1);
                              notificationIDs.add(NotificationPlugin().createID());
                            }
                            print(pickedTime);
                            print(dosage);
                            print(notificationIDs);
                          } else if (pickedTime.isNotEmpty) {
                            //if (!isEditMode) {
                            pickedTime.clear();
                            dosage.clear();
                            //notificationIDs.clear();
                            for (int i = 0; i < DayValue; i++) {
                              pickedTime.add(TimeOfDay.now());
                              dosage.add(1);
                              notificationIDs.add(NotificationPlugin().createID());
                            }
                            print(pickedTime);
                            print(dosage);
                            print(notificationIDs);
                          }
                          //}
                        });
                      }),
                ],
              ),
//              PanelTitle(
//                title: "Start Date",
//                isRequired: false,
//              ),
//              ListTile(
//                title: Text(
//                  getDate(startDate),
//                  style: TextStyle(
//                      fontSize: 19.0,
//                      fontWeight: FontWeight.w500,
//                      color: Color(0xFF3F51B5)),
//                ),
//                onTap: _pickstartDate,
//              ),
//              PanelTitle(
//                title: "End Date",
//                isRequired: false,
//              ),
//              ListTile(
//                title: Text(
//                  getDate(endDate),
//                  style: TextStyle(
//                      fontSize: 19.0,
//                      fontWeight: FontWeight.w500,
//                      color: Color(0xFF3F51B5)),
//                ),
//                onTap: _pickendDate,
//              ),
              Container(
                height: 40,
                width: 100,
                child: Visibility(
                  visible: isdayValueSelected,
                  child: PanelTitle(
                    title: "SET TIME AND DOSAGE",
                    isRequired: false,
                  ),
                ),
              ),
              Container(
                  height: 400,
                  width: 100,
                  child: Visibility(
                      visible: isdayValueSelected,
                      child: ListView.builder(
                        itemBuilder: (BuildContext context, int index) {
                          int num = index + 1;
                          return ListTile(
                            leading: Icon(
                                const IconData(0xe801, fontFamily: "Icons"),
                                color: Color(0xFF3F51B5)),
                            title: Text("${pickedTime[index].format(context)}",
                                style: TextStyle(fontSize: 19.0)),
                            subtitle: Text(
                                "${dosage[index]} ${returnDosage(widget.medicine.medicineType)}",
                                style: TextStyle(fontSize: 18.0)),
                            trailing: Text(num.toString(),
                                style: TextStyle(
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.w900,
                                    color: Colors.grey)),
                            onTap: () {
                              setState(() {
                                showAddTaskBottomSheet(context, index);
                              });
                            },
                          );
                        },
                        itemCount: DayValue,
                      ))),

              StreamBuilder(
                  stream: medicineFirestore().getOneMedicine(widget.medicine.medID),
                  builder:
                      (BuildContext context, AsyncSnapshot<List<Medicine>> snapshot) {
                    return RoundedButton(
                        title: "Complete",
                        colour: Color(0xFF3F51B5),
                        onPressed: () async {

                          int size = pickedTime.length;
                          String id = snapshot.data[0].id;

                          if (DayValue == null) {
                            _scaffoldKey.currentState.showSnackBar(SnackBar(
                                content: Text("Please Select How Many Times a day")));
                          } else {

                            int a = 0;
                            print("new");

                            while (a < size) {
                              DateTime datetime = DateTime(
                                  DateTime.now().year,
                                  DateTime.now().month,
                                  DateTime.now().day,
                                  pickedTime[a].hour,
                                  pickedTime[a].minute);

                              Alarm alarm = Alarm(
                                  dosage: dosage[a],
                                  notificationID: notificationIDs[a],
                                  medicineId: widget.medicine.medID,
                                  alarm: datetime);

                              medicineFirestore().addAlarm(alarm);
                              a++;
                            }

                            //DateTime datetime = DateTime.now();

                            Medicine m = new Medicine(
                                medicineName: widget.medicine.medicineName,
                                medicineType: widget.medicine.medicineType,
                                medID: widget.medicine.medID,
                                userID: widget.medicine.userID,
                                frequency: widget.medicine.frequency,
                                id:id,
                                //startDate:startDate,
                                //endDate:endDate,
                                intervalDay:widget.medicine.intervalDay,
                                specificDay:widget.medicine.specificDay,
                                alarmValue: size
                            );

                            print(size);

                            Navigator.pop(context);
                            await medicineFirestore().updateMedicine(m);


                            //bring to new page

                          }
                        });

                  }),

            ],
          ),
          // ),
          //),
        );
      }

  }

  returnDosage(String medType) {
    switch (medType) {
      case "MedicineType.Pill":
        {
          return "Dosage in Pill";
        }
        break;
      case "MedicineType.Bottle":
        {
          return "Dosage in mL";
        }
        break;
      case "MedicineType.Syringe":
        {
          return "Dosage in Injection";
        }
        break;
      case "MedicineType.Tablet":
        {
          return "Dosage in Tablet";
        }
        break;
    }
  }

  _getText(int value) {

    if (value == 1) {
      return Text("Once a day");
    } else if (value == 2) {
      return Text("Twice a day");
    }
    else {
      return Text("$value times a day");
    }
  }

  showAddTaskBottomSheet(BuildContext context, int index) async {
    int num = index + 1;
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return BottomSheet(
              onClosing: () {},
              builder: (BuildContext context) {
                return StatefulBuilder(
                    builder: (BuildContext context, setState) =>
                        SingleChildScrollView(
                            child: Container(
                          padding: EdgeInsets.only(
                              bottom: MediaQuery.of(context).viewInsets.bottom),
                          child: Container(
                            padding: EdgeInsets.all(25.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Text('Reminder $num',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 30.0,
                                        color: Color(0xFF3F51B5),
                                        fontWeight: FontWeight.bold)),
                                ListTile(
                                    leading: Text(
                                      'Medicine Time : ',
                                      style: TextStyle(
                                          fontSize: 19.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    title: Text(
                                        "${pickedTime[index].format(context)}",
                                        style: TextStyle(fontSize: 19.0)),
                                    trailing: Icon(Icons.keyboard_arrow_down),
                                    onTap: () {
                                      _pickTime(index);
                                    }),
                                SizedBox(height: 8),
                                Row(children: <Widget>[
                                  SizedBox(width: 12),
                                  Text(
                                      returnDosage(
                                          widget.medicine.medicineType),
                                      style: TextStyle(
                                          fontSize: 19.0,
                                          fontWeight: FontWeight.bold)),
                                ]),
                                SizedBox(height: 8),
                                Center(
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        FloatingActionButton(
                                          heroTag: "bt2",
                                          onPressed: () {
                                            setState(() {
                                              if (dosagenum != 0) dosagenum--;
                                              print(dosagenum);
                                              dosage[index] = dosagenum;
                                            });
                                          },
                                          child: new Icon(
                                              const IconData(0xe800,
                                                  fontFamily: "Icons"),
                                              color: Colors.black),
                                          backgroundColor: Colors.white,
                                        ),
                                        Text('$dosagenum',
                                            style: new TextStyle(
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold)),
                                        FloatingActionButton(
                                          heroTag: "bt1",
                                          onPressed: () {
                                            setState(() {
                                              dosagenum++;
                                              print(dosagenum);
                                              dosage[index] = dosagenum;
                                            });
                                            // add(index);
                                          },
                                          child: new Icon(Icons.add,
                                              color: Colors.black),
                                          backgroundColor: Colors.white,
                                        ),
                                      ]),
                                ),
                                SizedBox(height: 8),
                                RoundedButton(
                                  title: 'Add',
                                  colour: Color(0xFF3F51B5),
                                  onPressed: () {
                                    setState(() {
                                      dosage[index] = dosagenum;
                                    });
                                    print(dosage);
                                    print(pickedTime);
//                                int nid = NotificationPlugin().createID();
                                    Navigator.pop(context);
                                  },
                                )
                              ],
                            ),
                          ),
                        )));
              });
        });
  }

  _pickTime(int num) async {
    TimeOfDay time = await showTimePicker(
      context: context,
      initialTime: pickedTime[num],
    );

    if (time != null) {
      setState(() {
        pickedTime[num] = time;
        //print(pickedTime);
      });
    }
  }

  void showScheduledNotification(Medicine m, Alarm a) async {
    if(m.frequency=="Everyday")
    {
      await _scheduleEverydayNotification(a,m);
    }
    else if(m.frequency=="Day Interval")
      {
        await _scheduleIntervaldayNotification(a,m);
      }

  }

  Future<void> _scheduleEverydayNotification(Alarm alarm,Medicine med) async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        alarm.notificationID,
        med.medicineName,
        "dosage : "+alarm.dosage.toString(),
        _everyday(alarm),
        const NotificationDetails(
          android: AndroidNotificationDetails(
            'daily notification channel id',
            'daily notification channel name',
            'daily notification description',
            priority: Priority.high,
            importance: Importance.high,
            playSound: true,),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  Future<void> _scheduleIntervaldayNotification(Alarm alarm,Medicine med) async {
    await flutterLocalNotificationsPlugin.zonedSchedule(
        alarm.notificationID,
        med.medicineName,
        "dosage : "+alarm.dosage.toString(),
        _intervalday(alarm,med),
        const NotificationDetails(
          android: AndroidNotificationDetails(
            'daily notification channel id',
            'daily notification channel name',
            'daily notification description',
            priority: Priority.high,
            importance: Importance.high,
            playSound: true,),
        ),
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
        UILocalNotificationDateInterpretation.absoluteTime,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  tz.TZDateTime _intervalday(Alarm a,Medicine m) {

    int i = m.intervalDay;
    print ("what$i");
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, a.alarm.hour,a.alarm.minute);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(Duration(days: i));
    }
    return scheduledDate;
  }


  tz.TZDateTime _everyday(Alarm a) {
    final tz.TZDateTime now = tz.TZDateTime.now(tz.local);
    tz.TZDateTime scheduledDate =
    tz.TZDateTime(tz.local, now.year, now.month, now.day, a.alarm.hour,a.alarm.minute);
    if (scheduledDate.isBefore(now)) {
      scheduledDate = scheduledDate.add(const Duration(days: 1));
    }
    return scheduledDate;
  }



  Future<void> _checkPendingNotificationRequests() async {
    final List<PendingNotificationRequest> pendingNotificationRequests =
    await flutterLocalNotificationsPlugin.pendingNotificationRequests();
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content:
        Text('${pendingNotificationRequests.length} pending notification '
            'requests'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  Future<void> _cancelNotification(int nofid) async {
    await flutterLocalNotificationsPlugin.cancel(nofid);
  }

}
