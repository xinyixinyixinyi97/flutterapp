import 'package:flutter/material.dart';
import 'package:flutterapp/screens/RemindernChecklist.dart';
import 'package:provider/provider.dart';
import 'package:flutterapp/screens/authentications/authentic.dart';
import 'package:flutterapp/models/user.dart';


class Wrapper extends StatelessWidget {

  static const String id = 'wrapper_screen';

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    //String passuserid = user.uid;

    //print(user.uid);
    //return either Home or Authenticate widget
    if (user == null){
      return Authenticate();
    }else{
      return RemindernChecklist();
    }
  }
}
