import 'package:flutter/material.dart';
import 'package:flutterapp/Widgets/Blogtile.dart';
import 'package:flutterapp/helper/newsArticle_Data.dart';
import 'package:flutterapp/models/newsArticle_Model.dart';

class CategoryView extends StatefulWidget {

  final String category;

  const CategoryView({this.category});

  @override
  _CategoryViewState createState() => _CategoryViewState();
}

class _CategoryViewState extends State<CategoryView> {

  List<ArticleModel> articles = new List<ArticleModel>();
  bool _loading = true ;

  @override
  void initState() {

    super.initState();
    getCategoryArticles();
  }

  getCategoryArticles() async {
    newsCategory artClass = new newsCategory();
    await artClass.getArticles(widget.category);
    articles = artClass.art;
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          //change text widget.categary
          children: <Widget>[Text(widget.category)],
        ),
      ),
      body: _loading
          ? Center(
        child: CircularProgressIndicator(),
      ): 
      SingleChildScrollView(
        child: Container(
          child: Column(children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(16, 10,16,10),
              child: ListView.builder(
                  itemCount: articles.length,
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return BlogTile(
                      imageURL: articles[index].urlToImage,
                      title: articles[index].title,
                      desc: articles[index].description,
                      url: articles[index].url,
                    );
                  }),
            )
          ],
          ),
          ),
      ),
    );
  }
}
