import 'package:flutter/material.dart';
import 'package:flutterapp/screens/Medicine_NewEntry.dart';
import 'package:flutterapp/screens/mReminderDetails.dart';
import 'package:flutterapp/services/Medicine_firestore.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/models/Medicine_Model.dart';
import 'package:provider/provider.dart';

class MedicineHome extends StatefulWidget {
  @override
  _MedicineHomeState createState() => _MedicineHomeState();

  static const String id = 'MedicineHome_screen';

}

class _MedicineHomeState extends State<MedicineHome> {

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    String passuserid = user.uid.toString();

    return Scaffold(
        appBar: AppBar(
        backgroundColor: Color(0xFF3F51B5),
    elevation: 0.0,
    ),
    body: Container(
    color: Color(0xFFF6F8FC),
    child: Column(
    children: <Widget>[
    Flexible(
    flex: 3,
    child: TopContainer(passuserid),
    ),
    SizedBox(
    height: 10,
    ),
      Flexible(
          flex: 5,
          //child: Provider<GlobalBloc>.value(
            child: BottomContainer(passuserid),
            //value: _globalBloc,),
      ),
    ]
    ),
    ),
      floatingActionButton: FloatingActionButton(
        elevation: 4,
        backgroundColor: Color(0xFF3F51B5),
        child: Icon(
          Icons.add,
        ),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => MedicineNewEntry(),
            ),
          );
        },
      ),
    );
  }
}

class TopContainer extends StatelessWidget {

  final String uid;

  TopContainer(this.uid);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.elliptical(50, 27),
          bottomRight: Radius.elliptical(50, 27),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 5,
            color: Colors.grey[400],
            offset: Offset(0, 3.5),
          )
        ],
        color: Color(0xFF3F51B5),
      ),
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
              bottom: 10,
            ),
            child: Text(
              "MediReminder",
              style: TextStyle(
                fontFamily: "Angel",
                fontSize: 40,
                color: Colors.white,
              ),
            ),
          ),
          Divider(
            color: Color(0xFF9FA8DA),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.0),
            child: Center(
              child: Text(
                "Number of Mediminders",
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          StreamBuilder(
              stream: medicineFirestore().getMedicineList(uid),
              builder: (BuildContext context, AsyncSnapshot<List<Medicine>> snapshot){

                return Padding(
                  padding: EdgeInsets.only(top: 16.0, bottom: 5 ),
                  child: Center(
                    child: Text(
                      !snapshot.hasData ? '0' : snapshot.data.length.toString(),
                      style: TextStyle(
                        fontFamily: "Neu",
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ),
                );
              }

          )
        ],
      ),
    );
  }
}

class BottomContainer extends StatelessWidget{

  final String uid;

   BottomContainer(this.uid);

  @override
  Widget build(BuildContext context) {

    return StreamBuilder<List<Medicine>> (
        stream: medicineFirestore().getMedicineList(uid),
        builder: (BuildContext context, AsyncSnapshot<List<Medicine>> snapshot)
        {
        if (snapshot.connectionState == ConnectionState.active) {
          if (!snapshot.hasData) {
            return Container(
              color: Color(0xFFF6F8FC),
              child: Center(
                child: Text(
                  "Error",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xFFC9C9C9),
                      fontWeight: FontWeight.bold),
                ),
              ),
            );
          }
          else if (snapshot.data.length == 0) {
            return Container(
              color: Color(0xFFF6F8FC),
              child: Center(
                child: Text(
                  "Press + to add a Mediminder",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 24,
                      color: Color(0xFFC9C9C9),
                      fontWeight: FontWeight.bold),
                ),
              ),
            );
          }
          else {
            return Container(
              color: Color(0xFFF6F8FC),
              child: GridView.builder(
                padding: EdgeInsets.only(top: 12),
                gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  Medicine med = snapshot.data[index];
                  return MedicineCard(med);
                },
              ),
            );
          }
        }
        else {
        return Scaffold(
        body: Center(
        child: CircularProgressIndicator(),
        ),
        );
        }

        },

    );
  }
}

class MedicineCard extends StatelessWidget{

  final Medicine medicine;

  MedicineCard(this.medicine);

  Hero makeIcon(double size){
    if (medicine.medicineType == "MedicineType.Bottle") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe900, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    }
    else if (medicine.medicineType == "MedicineType.Pill") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe901, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    }
    else if (medicine.medicineType == "MedicineType.Syringe") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe902, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    }
    else if (medicine.medicineType == "MedicineType.Tablet") {
      return Hero(
        tag: medicine.medicineName + medicine.medicineType,
        child: Icon(
          IconData(0xe903, fontFamily: "Ic"),
          color: Color(0xFF3F51B5),
          size: size,
        ),
      );
    }
    return Hero(
      tag: medicine.medicineName + medicine.medicineType,
      child: Icon(
        Icons.error,
        color: Color(0xFF3F51B5),
        size: size,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding (
      padding : EdgeInsets.all(10.0),
      child: InkWell(
        highlightColor: Colors.white,
        splashColor: Colors.grey,
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  medicineReminderDetails(medicine: medicine),
            ),
          );
//          Navigator.of(context).push(
//              PageRouteBuilder<Null>
//            (
//            pageBuilder: (BuildContext context,Animation<double> animation,
//            Animation<double> secondaryAnimation
//            ){return AnimatedBuilder(
//                animation: animation,
//                builder: (BuildContext context, Widget child)
//                {
//                  return Opacity(
//                    opacity: animation.value,
//                    child: MedicineDetails(medicine),
//                  );
//                },
//            );},
//                transitionDuration: Duration(milliseconds: 500),
//          ));
        },
        child: Container(
          decoration : BoxDecoration(
            color: Colors.white10,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                makeIcon(80.0),
                Hero(
                  tag: medicine.medicineName,
                  child: Material(
                    color: Colors.transparent,
                    child: Text(
                      medicine.medicineName,
                      style: TextStyle(
                          fontSize: 22,
                          color: Color(0xFF3F51B5),
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
//                Text(
//                  medicine.interval == 1
//                      ? "Every " + medicine.interval.toString() + " hour"
//                      : "Every " + medicine.interval.toString() + " hours",
//                  style: TextStyle(
//                      fontSize: 16,
//                      color: Color(0xFFC9C9C9),
//                      fontWeight: FontWeight.w400),
//                )
              ],
            ),
          ),
        ),
      ),
    );
  }}





