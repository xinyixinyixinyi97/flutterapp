import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/services/auth.dart';
import 'package:flutterapp/Constants.dart';
import 'file:///C:/Users/hoyux/StudioProjects/flutterapp/lib/shared/loading.dart';


class SignIn extends StatefulWidget {

  final Function toggleView;
  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  //text field state
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return  loading ? Loading() : Scaffold(
      backgroundColor: Colors.brown[100],
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        elevation: 0.0,
        title: Text('Sign in to Vcare'),
        actions: <Widget>[
          FlatButton.icon(
            onPressed: (){
              widget.toggleView();
            },
            icon: Icon(Icons.person),
            label: Text('Register'),
          )
        ],
      ),
      body: Container(
          padding: EdgeInsets.symmetric(vertical: 20.0,horizontal: 50.0),
          child:Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0),
                TextFormField(
                    decoration: ktextInputDecoration.copyWith(hintText: 'Email'),
                    validator: (val)=> val.isEmpty ? 'Enter an email' : null,
                    onChanged: (val){
                      setState(()=>email = val);
                    }
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  decoration: ktextInputDecoration.copyWith(hintText: 'Password'),
                  obscureText: true,
                  validator: (val)=> val.length<8 ? 'Enter an password 8+ chars long' : null,
                  onChanged: (val){
                    setState(()=>password = val);

                  },
                ),
                SizedBox(height: 20.0),
                RaisedButton(
                    color: Colors.amber,
                    child: Text(
                      'Sign In',
                      style:TextStyle(color: Colors.black),
                    ),
                    onPressed: ()async{
                      if (_formKey.currentState.validate()){
                        setState(() =>loading = true);
                        dynamic result = await _auth.signInWithEmailAndPassword(email, password);
                        if(result == null){
                          setState(() {
                            error = 'Could not sign in with those credentials';
                            loading = false;
                          });
                        }
                      }
                    }
                ),
                SizedBox(height: 12.0),
                Text(
                  error,
                  style: TextStyle(color:Colors.red, fontSize: 14.0),
                ),
              ],
            ),
          )
      ),


    );
  }
}
