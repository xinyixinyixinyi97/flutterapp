import 'dart:async';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/models/checklistTask_Model.dart';

class Home extends StatefulWidget {

  final String userId;
  Home({Key key, this.userId}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Task> _todoList = new List();
  final FirebaseDatabase _database = FirebaseDatabase.instance;

  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;

  Query _todoQuery;

  @override
  void initState() {
    super.initState();
    _todoList = new List();

    _todoQuery = _database
        .reference()
        .child("todo")
        .orderByChild("userId")
        .equalTo(widget.userId);

    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(onEntryAdded);
    _onTodoChangedSubscription = _todoQuery.onChildChanged.listen(onEntryChanged);
  }

  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    _onTodoChangedSubscription.cancel();
    super.dispose();
  }

  onEntryChanged(Event event) {
    var oldEntry = _todoList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _todoList[_todoList.indexOf(oldEntry)] = Task.fromSnapshot(event.snapshot);
    });
  }

  onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Task.fromSnapshot(event.snapshot));
    });
  }

  addNewTask(String todoItem) {
    if (todoItem.length > 0) {
      Task todo = new Task(
          task: todoItem.toString(), isDone: false, userid: widget.userId);
      _database.reference().child("todo").push().set(todo.toJson());
    }
  }

  updateTask(Task todo) {
    //Toggle completed
    todo.isDone = !todo.isDone;
    if (todo != null) {
      _database.reference().child("todo").child(todo.key).set(todo.toJson());
    }
  }

  deleteTask(String todoId, int index) {
    _database.reference().child("todo").child(todoId).remove().then((_) {
      print("Delete $todoId successful");
      setState(() {
        _todoList.removeAt(index);
      });
    });
  }

  showAddTaskBottomSheet(BuildContext context) async {
    _textEditingController.clear();
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) => SingleChildScrollView(
                child: Container(
              padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom),
              child: Container(
                padding: EdgeInsets.all(25.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text('Add Task',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 32.0, color: Colors.blueAccent)),
                    TextField(
                      controller: _textEditingController,
                      autofocus: true,
                      textAlign: TextAlign.center,
//                  onChanged: (newText) {
//                    //newTaskname = newText;
//                    print (newTaskname);
//                  },
                      style: TextStyle(fontSize: 20.0),
                      decoration: InputDecoration(
                        hintText: 'Enter new task',
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.blueAccent),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.blueAccent),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    FlatButton(
                      child: Text('Add',
                          style:
                              TextStyle(color: Colors.white, fontSize: 18.0)),
                      color: Colors.blueAccent,
                      onPressed: () {
                        addNewTask(_textEditingController.text.toString());
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            )));
  }

  Widget showTaskList() {
    if (_todoList.length == 0)
      return Center(child: CircularProgressIndicator());
    if (_todoList.length > 0) {
      return ListView.builder(
        shrinkWrap: true,
        itemCount: _todoList.length,
        itemBuilder: (BuildContext context, int index) {
          String taskId = _todoList[index].key;
          String subject = _todoList[index].task;
          bool completed = _todoList[index].isDone;
          //String userId = _todoList[index].userid;

          return ListTile(
            title: Text(subject,
                //style: TextStyle(fontSize: 20.0),
                style: TextStyle(
                    fontSize: 25.0,
                    decoration: completed ? TextDecoration.lineThrough : null)),
            leading: IconButton(
                icon: (completed)
                    ? Icon(
                        Icons.done_outline,
                        color: Colors.blueAccent,
                        size: 35.0,
                      )
                    : Icon(Icons.done, color: Colors.grey, size: 35.0),
                onPressed: () {
                  updateTask(_todoList[index]);
                }),
            trailing: IconButton(
              key: Key(taskId),
              icon: Icon(Icons.delete),
              color: Colors.red,
              onPressed: () {
                deleteTask(taskId, index);
              },
            ),
          );
        },
      );
    } else {
      return Center(
          child: Text(
        "No Task",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 30.0),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            'Checklist',
            style: TextStyle(fontSize: 25),
          ),
        ),
        body: showTaskList(),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            showAddTaskBottomSheet(context);
          },
          tooltip: 'Increment',
          icon: Icon(Icons.add, size: 30.0),
          label: Text('Create New Item into Checklist',
              style: TextStyle(fontSize: 19.0)),
          backgroundColor: Colors.blueAccent,
          shape: RoundedRectangleBorder(),
        ));
  }
}
