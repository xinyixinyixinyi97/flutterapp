import 'package:flutter/material.dart';
import 'file:///C:/Users/hoyux/StudioProjects/flutterapp/lib/models/Areminder.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/screens/AddAppointmentPage.dart';
import 'package:flutterapp/services/Fstore.dart';
import 'package:provider/provider.dart';
import '../Constants.dart';
import 'AppDetailsPage.dart';

class Areminder extends StatelessWidget {
  static const String id = 'AReminder_screen';

  @override
  Widget build(BuildContext context) {

    final user = Provider.of<User>(context);
    String passuserid = user.uid.toString();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Row(
          children: <Widget>[Text('Reminder')],
        ),
      ),
      body: StreamBuilder(
          stream: FirestoreService().getAppointment(passuserid),
          builder: (BuildContext context,
              AsyncSnapshot<List<Appointment>> snapshot) {

            if (snapshot.hasError || !snapshot.hasData)
              return Center(child: CircularProgressIndicator());
            return ListView.builder(

                itemCount: snapshot.data.length,
                itemBuilder: (context, int index) {

                  Appointment app = snapshot.data[index];

                  return Container(
                    height: 100,
                    child: Card(
                      color: Colors.lightBlue[100],
                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white, width: 1),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      shadowColor: Colors.black,
                      child: ListTile(
                        title: Text(app.APName,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold)),


                        subtitle: Text(
                          getDateandTime(app.APDate),
                          style: TextStyle(color: Colors.black, fontSize: 18.0),
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ButtonTheme(
                              minWidth: 12.0,
                              child: RaisedButton(
                                color: Colors.lightBlue,
                                child: Text(
                                  'Edit',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) =>
                                          AddAppointmentPage(app: app),
                                    )),
                              ),
                            ),
                            IconButton(
                              color: Colors.black,
                              icon: Icon(Icons.delete),
                              onPressed: () => _deleteApp(context, app.id),
                            ),
                          ],
                        ),
                        onTap: () => Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => AppointmentDetailsPage(app: app,))),
                      ),
                    ),
                  );
                });
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => AddAppointmentPage()));
        },
      ),
    );
  }

  void _deleteApp(BuildContext context, String id) async {
    if (await _showConfirmationDialog(context)) {
      try {
        await FirestoreService().deleteAreminder(id);
      } catch (e) {
        print(e);
      }
    }
  }

  Future<bool> _showConfirmationDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) => AlertDialog(
              content: Text("Are you sure you want to delete?"),
              actions: <Widget>[
                FlatButton(
                  textColor: Colors.red,
                  child: Text("Delete"),
                  onPressed: () => Navigator.pop(context, true),
                ),
                FlatButton(
                  textColor: Colors.black,
                  child: Text("No"),
                  onPressed: () => Navigator.pop(context, false),
                ),
              ],
            ));
  }
}

