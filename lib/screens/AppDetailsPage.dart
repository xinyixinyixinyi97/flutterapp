import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file:///C:/Users/hoyux/StudioProjects/flutterapp/lib/models/Areminder.dart';
import 'package:flutterapp/services/Fstore.dart';

import '../Constants.dart';

class AppointmentDetailsPage extends StatefulWidget {
  // ignore: non_constant_identifier_names
  final Appointment app;

  const AppointmentDetailsPage({Key key, this.app}) : super(key: key);

  @override
  _AppointmentDetailsPageState createState() => _AppointmentDetailsPageState();
}

class _AppointmentDetailsPageState extends State<AppointmentDetailsPage> {


  Widget build(BuildContext context) {

//    String a = FirestoreService().getDate(app.APDate);
//    String b = DateFormat.jm().format(app.APDate);

    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.app.APName,
              style: Theme.of(context)
                  .textTheme
                  // ignore: deprecated_member_use
                  .title
                  .copyWith(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
            const SizedBox(height: 20.0),
            Text(
              widget.app.APDetails,
              style: TextStyle(fontSize: 16.0),
            ),
            const SizedBox(height: 20.0),
            Text(
              getDate(widget.app.APDate),
              style: TextStyle(fontSize: 16.0),
            ),
            const SizedBox(height: 20.0),
            Text(
              getTime(widget.app.APDate),
              style: TextStyle(fontSize: 16.0),
            ),
            const SizedBox(height: 20.0),
            Row(
              children: <Widget>[
                Text(
                  'Remind you ',
                  style: TextStyle(fontSize: 16.0),
                ),
                Text(
                  widget.app.reminder,
                  style: TextStyle(fontSize: 16.0),
                ),
                Text(' '),
                Text(
                  widget.app.dhm,
                  style: TextStyle(fontSize: 16.0),
                ),
                Text(
                  ' before appointment',
                  style: TextStyle(fontSize: 16.0),
                ),
              ],
            ),

//            FlatButton(
//              color: Colors.blue,
//              onPressed: (){
//                showScheduledNotification();
//              },
//              child: Text("show Notification"),
//            ),
          ],
        ),
      ),

    );
  }

}

