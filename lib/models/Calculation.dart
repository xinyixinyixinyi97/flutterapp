
class Calculation {
  static bmiIndex(num height,num weight){
    String tag ;
    num dbHeight = height/100;
    num bmi = (weight)/(dbHeight*dbHeight);
    double bmiRound = double.parse((bmi).toStringAsFixed(2));

    return bmiRound.toString();

  }

  static bmiCal(num height,num weight){
    String tag ;
    num dbHeight = height/100;
    num bmi = (weight)/(dbHeight*dbHeight);
     double bmiRound = double.parse((bmi).toStringAsFixed(2));

    if(bmiRound<5){
      tag='N/A';
          return tag;
    }
    if (bmiRound<18.5){
      tag = 'Underweight';
      print(bmiRound.toString());
      return tag;
    }
    if(bmiRound>=18.5 && bmiRound<25){
      tag = 'Normal Weight';
      print(bmiRound.toString());
    return tag;}
    if (bmiRound>=25){
      tag = 'Overweight';
      print(bmiRound.toString());}
    return tag;

  }

  static bpCal (num sys,num dia) {
    if (sys <=90 && dia <=60)
      return 'Low BP';

    if (sys < 120 && sys>90 ){
      if(dia<90 && dia>60)
        return 'Normal BP';
    }
    if (sys <140 && sys >=120) {
      if (dia < 90 && dia >= 80)
        return 'High-normal BP';
    }
    if(sys >=140 && dia <90){
      return 'Isolated Systolic Hypertension';
    }
    if (sys >=140 || dia >=90 ){
      return 'High BP - Severe';
    }
    else
      return 'Abnormal Value';
  }
  static bgCal (String status, num bg){
    if(status =='Fasting'){
      if(bg>= 4 && bg <5.6 )
        return 'Normal Glucose Level';
      if(bg>= 5.6 && bg <7 )
        return 'Normal Glucose Level';
      if(bg<4)
        return 'Low Blood Sugar';
      if (bg >= 7)
        return 'High Blood Sugar';
    }
    if(status =='Non-Fasting'){
      if(bg>=7.8 && bg <11.1 )
        return 'Normal Glucose Level';
      if(bg<7.8)
        return 'Low Blood Sugar';
      if (bg >=11.1)
        return 'High Blood Sugar';
    }


  }

}