class Medicine {
  //final List<dynamic> alarmIDs;
  final String medicineName;
  final String medicineType;
  final int intervalDay;
  final String medID;
  final String userID;
  final String frequency;
  final int alarmValue;
  final List<dynamic> specificDay;
  final String id;

  Medicine(
      {this.medicineName,
      this.medicineType,
      this.intervalDay,
      this.medID,
      this.userID,
      this.frequency,
      this.alarmValue,
      this.specificDay,
      this.id});

  //from map convert to medicine object
  Medicine.fromMap(Map<String, dynamic> obj, String id)
      : medicineName = obj["medicineName"],
        medicineType = obj["medicineType"],
        intervalDay = obj["intervalDay"],
        medID = obj["medID"],
        userID = obj["userID"],
        frequency = obj["frequency"],
        alarmValue = obj["alarmValue"],
        specificDay = obj["specificDay"],
        id = id;

  Map<String, dynamic> toMap() {

    return {
      "medicineName": medicineName,
      "medicineType": medicineType,
      "userID": userID,
      "frequency": frequency,
      "alarmValue": alarmValue,
      "intervalDay": intervalDay,
      "specificDay": specificDay,
      //Primary key
      "medID": medID
    };
  }
}
