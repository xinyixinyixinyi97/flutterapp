import 'package:firebase_database/firebase_database.dart';

class Task {

  String key;
  String task;
  bool isDone;
  String userid;

  Task({this.task, this.isDone,this.userid});

  Task.fromSnapshot(DataSnapshot snapshot):
        key = snapshot.key,
        userid = snapshot.value['userId'],
        task = snapshot.value['task'],
        isDone = snapshot.value['isChecked'];

  toJson() {
    return {
      'userId': userid,
      'task' : task,
      'isChecked' : isDone,
    };
  }


}


