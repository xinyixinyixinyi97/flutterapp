class Alarm {
  final int notificationID;
  final int dosage;
  final String medicineId;
  final DateTime alarm;
  final String alarmID;

  Alarm(
      {this.notificationID,
      this.dosage,
      this.medicineId,
      this.alarm,
      this.alarmID});

  //from map convert to medicine object
  Alarm.fromMap(Map<String, dynamic> data, String alarmID)
      :
        notificationID = data["notificationID"],
        dosage = data["dosage"],
        medicineId = data["medicineId"],
        alarmID = alarmID,
        alarm = data["alarm"].toDate();

  Map<String, dynamic> toMap() {
    return {
      "notificationID": notificationID,
      "dosage": dosage,
      "medicineId": medicineId,
      "alarm": alarm
    };
  }
}
