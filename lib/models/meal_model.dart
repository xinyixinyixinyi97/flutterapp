class Meal {
  int id;
  String imageBack;
  String title;
  int readyInMinutes;
  int servings;
  String sourceUrl;
  String imageBack1;

  Meal(
      {this.id,
      this.imageBack,
      this.title,
      this.readyInMinutes,
      this.servings,
      this.sourceUrl,
      this.imageBack1});

  //Map List oif json file from API to Vcare Mealplan
  Meal.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    imageBack = json['id'].toString() + '-480x360.' + json['imageType'];
    title = json['title'];
    readyInMinutes = json['readyInMinutes'];
    servings = json['servings'];
    sourceUrl = json['sourceUrl'];
    imageBack1 = "https://spoonacular.com/recipeImages/"+json['id'].toString() + '-480x360.' + json['imageType'];
  }
}

//
//class Meal {
//  int id;-
//  String imageType;
//  String title;-
//  int readyInMinutes;
//  int servings;
//  String sourceUrl;
//
//  Meal(
//      {this.id,
//        this.imageType,
//        this.title,
//        this.readyInMinutes,
//        this.servings,
//        this.sourceUrl});
//
//Meal.fromMap(Map<String, dynamic> json) {
//id = json['id'];
//imageType = json['imageType'];
//title = json['title'];
//readyInMinutes = json['readyInMinutes'];
//servings = json['servings'];
//sourceUrl = json['sourceUrl'];
//}
//}
