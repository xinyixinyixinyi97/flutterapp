class Appointment{

  final String APName;
  final String APDetails;
  final DateTime APDate;
  final String id;
  final String userID;
  final String reminder;
  final String dhm;
  final int notificationID;

  // ignore: non_constant_identifier_names
  Appointment({this.APName, this.APDetails,this.APDate,this.id,this.userID,this.reminder,this.dhm,this.notificationID});

  //fromMap convert to Appointment obj
  Appointment.fromMap(Map<String,dynamic> data, String id):
    APName = data["appointmentname"],
    APDetails = data["appointmentdetails"],
    APDate = data["appointmentdate"].toDate(),
    id = id,
    userID = data["userID"],
    reminder = data["setreminder"],
    dhm = data["dhm"],
    notificationID = data["notificationID"];

  Map<String,dynamic> toMap(){

    return{
      "appointmentname" : APName,
      "appointmentdetails" : APDetails,
      "appointmentdate" : APDate,
      "userID" : userID,
      "setreminder" : reminder,
      "dhm" : dhm,
      "notificationID" : notificationID,
    };

  }

}

