import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/models/InstructionID.dart';
import 'package:flutterapp/models/checklistTask_Model.dart';
import 'package:flutterapp/models/user.dart';
import 'package:flutterapp/screens/Checklist.dart' as main;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutterapp/Pages/Food/ProductionInfor.dart';
import 'package:flutterapp/models/IngredientID.dart';
import 'package:flutterapp/models/NutrientID.dart';
import 'package:flutterapp/services/database.dart';
import 'package:provider/provider.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class RecipeIdPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: RecipeIdPageMain());
  }
}

class RecipeIdPageMain extends StatefulWidget {
  final image;
  final int id;
  final String title;
  final String userid;
  final isFindButton = true;

  const RecipeIdPageMain({Key key, this.image, this.id, this.title, this.userid})
      : super(key: key);


  @override
  _RecipeIdPageMainState createState() => _RecipeIdPageMainState();
}

class _RecipeIdPageMainState extends State<RecipeIdPageMain> {
  String title;
  String baseUrl = "https://api.spoonacular.com/recipes/";
  String keyID =
      "/ingredientWidget.json?apiKey=30099e27b78c4b36bcfdf3070645383a";
  String url;
  String baseUrl2 = "https://api.spoonacular.com/recipes/";
  String keyID2 =
      "/nutritionWidget.json?apiKey=30099e27b78c4b36bcfdf3070645383a";
  String url2;
  String url3 ='/analyzedInstructions?apiKey=30099e27b78c4b36bcfdf3070645383a';
  Future<IngredientID> futureIngre;
  Future<AnalyzedInstructions> futureRecipeInstructions;
  Future<bool> fav;
  bool toChecklist;

  @override
  void initState() {
    super.initState();
    futureRecipeInstructions = fetchAnalyzedInstructions(widget.id);
    fav=getAdded(widget.id,widget.userid);
    toChecklist=false;
  }

  Future<AnalyzedInstructions> fetchAnalyzedInstructions(int id) async {

    final response = await http.get(baseUrl+id.toString()+url3);

    if (response.statusCode == 200) {
      return AnalyzedInstructions.fromJson(jsonDecode((response.body))[0]);
    } else {
      throw Exception('Failed to load recipe');
    }
  }

  Future<IngredientID> _fecthSearch() async {
    http.Response response =
    await http.get(baseUrl + widget.id.toString() + keyID);
    print(baseUrl + widget.id.toString() + keyID);
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return IngredientID.fromJson(json);
    } else {
      throw Exception('failed to load search recipe');
    }
  }


  Future<NutrientID> _fecthSearchNutrient() async {
    http.Response response =
    await http.get(baseUrl2 + widget.id.toString() + keyID2);
    print('search nutrient: ${baseUrl2 + widget.id.toString() + keyID2}');
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return NutrientID.fromJson(json);
    } else {
      throw Exception('failed to load search nutrients recipe');
    }
  }

  @override
  Widget build(BuildContext context) {


    final user = Provider.of<User>(context);
    Widget cancelButton = FlatButton(
      child: Text("Cancel",style: TextStyle(fontSize: 21)),
      onPressed:  () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Generate",style: TextStyle(fontSize: 21)),
      onPressed:  () {
        if(toChecklist==false){
        setState(() {
          toChecklist=true;
        });}
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Add To Checklist",style: TextStyle(fontSize: 26)),
      content: Text("Generate grocery checklist from this recipe?",style: TextStyle(fontSize: 22)),
      actions: [
        cancelButton,
        continueButton,
      ],
    );
    addNewTask(String todoItem) {
      if(toChecklist=true){
      if (todoItem.length > 0) {
        Task todo = new Task(
            task: todoItem.toString(), isDone: false, userid: user.uid);
        FirebaseDatabase.instance.reference().child("todo").push().set(todo.toJson());
      }}
    }
    return Scaffold(
        appBar: AppBar(title: Text("Recipe",style: TextStyle(fontSize: 25),),
          backgroundColor: Colors.lightBlue[900],
          actions: <Widget>[
          FlatButton(
              onPressed: ()  {
                  DatabaseService(uid: user.uid).updateRecipeData(widget.image.toString(), widget.id, widget.title);
                  setState(() {});
              },
              child: Row(
                children: [
                  Text(
                    'Save',
                    style: TextStyle(fontSize: 23, color: Colors.yellow,fontWeight: FontWeight.w900),
                  ),

                ],
              )),
        ],),
        body: SafeArea(
          child: ListView(
            children: <Widget>[
              Stack(children: <Widget>[
                Hero(
                  tag: widget.image,
                  child: Container(
                      height: 250,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(widget.image),
                              fit: BoxFit.cover)),
                      child: Container(
                          decoration: BoxDecoration(
                          ))),
                ),
                Positioned(
                    top: 100,
                    child: Container(
                        padding: EdgeInsets.all(10),
                        height: 70,
                        width: MediaQuery.of(context).size.width,
                        child: Align(
                          alignment: Alignment.center,

                        )))
              ]),
              Container(
                padding: EdgeInsets.all(10),
                height: MediaQuery.of(context).size.height+110,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.title,style: TextStyle(fontSize: 28),),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Ingredient',
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                                color: Colors.black87)),
                        FlatButton(onPressed: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return alert;
                            },
                          );
                        }
                        ,
                          child: Row(
                            children: [
                              Text("Add ",style: TextStyle(fontSize: 21)),
                              Icon(
                                Icons.local_grocery_store_outlined,
                                color:Colors.blue,
                                size: 30,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Container(
                        height: 100,
                        width: double.infinity,
                        child: FutureBuilder(
                            future: _fecthSearch(),
                            builder:
                                (BuildContext context, AsyncSnapshot snapshot) {
                              if (snapshot.hasError) {
                                return new Text(
                                    'error: ${snapshot.error.toString()}');
                              } else if (snapshot.hasData) {
                                IngredientID ingre = snapshot.data;
                                return ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: ingre.ingredients.length,
                                    itemBuilder: (context, index) {

                                      if (toChecklist == true) {
                                        print("checklistTrue");

                                        print(ingre.ingredients.length);
                                        for (int i = 0; i <
                                            ingre.ingredients.length; i++) {
                                          print(i);
                                          addNewTask(
                                              snapshot.data.ingredients[i]
                                                  .name + " x " + snapshot.data
                                                  .ingredients[i].amount
                                                  .metric.value.toString() +
                                                  " " + ingre.ingredients[i]
                                                  .amount.metric.unit);
                                        }
                                        toChecklist=false;

                                      }

                                      return Container(
                                        width: 200,
                                        margin: EdgeInsets.only(right: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.blue.shade100),
                                        child: ListTile(
                                          title: Text(snapshot
                                              .data.ingredients[index].name,style: TextStyle(fontSize: 21),),
                                          subtitle: Text(
                                              '${ingre.ingredients[index].amount.metric.value.toString() + " " + ingre.ingredients[index].amount.metric.unit}',style: TextStyle(fontSize: 20),),
                                        ),
                                      );
                                    });
                              }

                              return Center(
                                  child: new CircularProgressIndicator(
                                    strokeWidth: 1,
                                  ));
                            })),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Instructions',style: TextStyle(fontSize: 26,fontWeight: FontWeight.bold)),
                    _getInstructionsWidget(),
                    Text("source : https://api.spoonacular.com/")
                  ],
                ),
              ),
            ],
          ),
        ));
  }
  //rout to move to other screen
  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => ProductInforPage(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
        return  SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
  Widget  _getInstructionsWidget(){
         return Container(
              height:550,
         child: SingleChildScrollView(
           child: FutureBuilder<AnalyzedInstructions>(
             future: futureRecipeInstructions,
             builder: (context,snapshot){
               print("snapshot: ${snapshot.data.toString()}");
               if (snapshot.connectionState == ConnectionState.done &&
                   snapshot.hasData) {
                 final steps = snapshot.data.steps;
                 return Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: ListView.builder( scrollDirection: Axis.vertical,
                       shrinkWrap: true,
                       physics: NeverScrollableScrollPhysics(),
                       itemCount: snapshot.data.steps.length,
                       itemBuilder: (context,index){
                       return Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Text("${steps[index].number}. ${steps[index].step}",style: TextStyle(fontSize: 23),),
                           SizedBox(height: 20,),
                         ],
                       );
                       }),
                 );
               }else if (snapshot.hasError){
                 return  Text("${snapshot.error}");
               }
               return CircularProgressIndicator();

             },
           ),
         ),
         );


  }


  Widget _buildNutrition({String text,IconData icon,Color color}){
    return Container(
        height:50,
        margin: EdgeInsets.only(right:10),
        width:125,
        decoration:BoxDecoration(
          border: Border.all(
            color:Colors.blue,
            width:1,

          )
          ,borderRadius: BorderRadius.circular(5),
        ),child:Row(
      children: <Widget>[
        SizedBox(
          width: 5,
        ),
        Container(
            width:35,
            height:35,
            decoration:BoxDecoration(
              shape:BoxShape.circle,
              color:color,
            ),
            child: Center(child: Icon(icon,color:Colors.white))
        ),
        SizedBox(
          width: 10,
        ),
        Text(text,style:TextStyle(fontSize:14,fontWeight: FontWeight.w400,color:Colors.black54)),
      ],
    )
    );
  }
  Widget _buildGoodNutrient(){
    return Column(
      children: <Widget>[
        Container(
          height:20,
          width:MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildTitle(title:'Mineral',color:Colors.blue.shade200),
              _buildTitle(title:'Amount',color:Colors.blue.shade200),
              _buildTitle(title:'DailyNeeds',color:Colors.blue.shade200),

            ],
          ),
        ),
        Container(
          height:300,
          width:double.infinity,
          child: FutureBuilder(
            future: _fecthSearchNutrient(),
            builder: (BuildContext context,AsyncSnapshot snapshot){
              NutrientID nutrient = snapshot.data;
              if(snapshot.hasData){
                return ListView.builder(
                    itemCount: nutrient.good.length,
                    itemBuilder: (context,index){
                      return Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  children: <Widget>[
                                    Text('${nutrient.good[index].title.toString()}'),
                                  ],
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('${nutrient.good[index].amount.toString()}'),
                                  ],
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  children: <Widget>[
                                    Text('${nutrient.good[index].percentOfDailyNeeds.toString()}'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Divider(
                              color:Colors.blue.shade400
                          ),
                        ],
                      );
                    });
              }else if(snapshot.hasError){
                return new Text('An error occur: ${snapshot.error}');
              }
              return  Center(child: new CircularProgressIndicator(
                strokeWidth: 1,
              ));
            },

          ),
        )
      ],
    );

  }
  Widget _buildBaddNutrient(){
    return Column(
      children: <Widget>[
        Container(
          height:20,
          width:MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildTitle(title:'Mineral',color:Colors.red.shade100),
              _buildTitle(title:'Amount',color:Colors.red.shade100),
              _buildTitle(title:'DailyNeeds',color:Colors.red.shade100),

            ],
          ),
        ),
        Container(
          height:250,
          width:double.infinity,
          child: FutureBuilder(
            future: _fecthSearchNutrient(),
            builder: (BuildContext context,AsyncSnapshot snapshot){
              NutrientID nutrient = snapshot.data;
              if(snapshot.hasData){
                return ListView.builder(
                    itemCount: nutrient.bad.length,
                    itemBuilder: (context,index){
                      return Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container( width:MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  children: <Widget>[
                                    Text('${nutrient.bad[index].title.toString()}'),
                                  ],
                                ),
                              ),
                              Container( width:MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text('${nutrient.bad[index].amount.toString()}'),
                                  ],
                                ),
                              ),
                              Container(
                                width:MediaQuery.of(context).size.width/3-20,
                                child: Column(
                                  children: <Widget>[
                                    Text('${nutrient.bad[index].percentOfDailyNeeds.toString()}'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Divider(
                            color: Colors.red.shade400,
                          ),
                        ],
                      );
                    });
              }else if(snapshot.hasError){
                return new Text('An error occur: ${snapshot.error}');
              }
              return  Center(child: new CircularProgressIndicator(
                strokeWidth: 1,
              ));
            },

          ),
        )
      ],
    );

  }
  //build title for list good nutrient
  Widget _buildTitle({String title,Color color}){
    return Container(
        width:MediaQuery.of(context).size.width/3-20,
        decoration:BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color:color
        ),
        child:Center(child:Text(title,style:TextStyle(color:Colors.black87,fontSize:14,fontWeight:FontWeight.bold)))
    );
  }

  Future<bool> getAdded(int id,String uid) async {
    String color ;
    bool fav = false;
    try{
      await Firestore.instance.collection('UserData').document(uid).collection('favouriteRecipe').document(id.toString()).get().then((doc) {
        if (doc.exists){
          fav = true;
          color = Colors.pinkAccent as String;}
        else{
          fav = false;
        color = Colors.blueGrey as String;}
      });
      return fav;
    }catch(e){
      return false;
    }

  }
}
