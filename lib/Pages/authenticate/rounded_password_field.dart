import 'package:flutter/material.dart';
import 'package:flutterapp/Pages/authenticate/text_field_container.dart';
import 'package:flutterapp/shared/constants.dart';

class RoundedPasswordField extends StatelessWidget {
  final ValueChanged<String> onChanged;
  const RoundedPasswordField({
    Key key,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(style: TextStyle(fontSize: 22),
        obscureText: true,
        validator: (val)=> val.length<8 ? 'Password should 8+ chars long' : null,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          hintText: "Password",errorStyle: TextStyle(fontSize: 16),
          icon: Icon(
            Icons.lock,
            color: kPrimaryColor,
          ),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
