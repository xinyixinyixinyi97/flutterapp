import 'package:flutter/material.dart';
import 'package:flutterapp/Pages/authenticate/text_field_container.dart';
import 'package:flutterapp/shared/constants.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(style: TextStyle(fontSize: 22),
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        validator: (val)=> val.isEmpty ? 'Enter your email correctly' : null,
        decoration: InputDecoration(

          icon: Icon(
            icon,
            color: kPrimaryColor,
          ),
          hintText: hintText,errorStyle: TextStyle(fontSize: 16),
          border: InputBorder.none,
        ),
      ),
    );
  }
}
