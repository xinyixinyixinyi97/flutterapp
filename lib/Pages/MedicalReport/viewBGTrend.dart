import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutterapp/shared/loading.dart';
import 'package:flutterapp/services/database.dart';
import 'package:flutterapp/models/user.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'BGList.dart';
import 'editBGform.dart';

class viewBGTrend extends StatefulWidget {
  @override
  _viewBGTrendState createState() => _viewBGTrendState();
}

class _viewBGTrendState extends State<viewBGTrend> {
  FlutterLocalNotificationsPlugin localNotification;
  @override
  void initState(){
    super.initState();
    var androidInitialize = new AndroidInitializationSettings('app_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(android: androidInitialize,iOS:iOSInitialize );
    localNotification = new FlutterLocalNotificationsPlugin();
    localNotification.initialize(initializationSettings);
  }

  Future _showNotification()async{
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotification.show(0,"Health Records Reminder", "Have you take your record today?", generalNotificationDetails);

  }
  Future _showScheduleNotification()async{
    var time = Time(10,30,0);
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    localNotification..showDailyAtTime(0,"Daily Records Reminder", "Have you take your record today?",time ,generalNotificationDetails);
  }
  Future _showScheduleNotification1()async{
    var time = Time(18,30,0);
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    localNotification..showDailyAtTime(0,"Daily Records Reminder", "Have you take your record today?",time ,generalNotificationDetails);
  }
  createAlertDialog(BuildContext context) {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text("Blood Glucose Tips"),
        content: SingleChildScrollView(
          child: Container(
            height: 550,
            width: 600,
            child: ListView(children: <Widget>[
              Center(
                  child: Text(
                    'FBS (Fasting Blood Sugar)',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  )),
              Text('Blood sample for this test is taken after an 8-12 hours or overnight fasting. Water intake is allowed during this fasting period. ',style: TextStyle(fontSize: 20),),
              DataTable(
                columns: [
                  DataColumn(label: Text(
                      'Value(mmol/L)',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                  )),
                  DataColumn(label: Text(
                      'Result',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                  )),
                ],
                rows: [
                  DataRow(cells: [
                    DataCell(Text('less than 5.6')),
                    DataCell(Text('Normal')),
                  ]),
                  DataRow(cells: [
                    DataCell(Text('5.6 to 6.9')),
                    DataCell(Text('Pre-diabetes')),
                  ]),
                  DataRow(cells: [
                    DataCell(Text('7 and above')),
                    DataCell(Text('Diabetes')),
                  ]),
                ],
              ),
              Center(
                  child: Text(
                    'RBS (Random Blood Sugar)',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  )),
              Text('This test measures blood sugar level at random. This means sampling is done at anytime of the day regardless of diet taken.'),
              DataTable(
                columns: [
                  DataColumn(label: Text(
                      'Value(mmol/L)',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                  )),
                  DataColumn(label: Text(
                      'Result',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                  )),
                ],
                rows: [
                  DataRow(cells: [
                    DataCell(Text('7.8 - 11.1')),
                    DataCell(Text('Normal')),
                  ]),
                  DataRow(cells: [
                    DataCell(Text('more than 11.1')),
                    DataCell(Text('Diabetes')),
                  ]),
                ],
              ),
            ]),
          ),
        )  ,
        actions: <Widget>[MaterialButton(
          child: Text('Close'),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    void _showEditBGPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.symmetric(vertical: 60.0, horizontal: 40.0),
              child: Column(
                children: [
                  EditBGForm(),
                ],
              ),
            );
          },
          isScrollControlled: true);
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          centerTitle: true,
          title: Text('Blood Glucose',
              style: TextStyle(
                fontSize: 25,
              )),
          backgroundColor: Colors.blue[900],
          bottom: TabBar(
            tabs: [
              Tab(
                  icon: Icon(Icons.article_outlined),
                  child: Text(
                    "Track",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
              Tab(
                  icon: Icon(Icons.stacked_line_chart),
                  child: Text(
                    "Trends",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showEditBGPanel(),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          label: Text(
            'Record manually',
            style: TextStyle(fontSize: 22),
          ),
          // child: Container(
          //   constraints: BoxConstraints(
          //       maxWidth: 300.0, minHeight: 50.0),
          //   alignment: Alignment.center,
          //   child: Text(
          //     "Record manually",
          //     textAlign: TextAlign.center,
          //     style: TextStyle(
          //         fontSize: 22, color: Colors.white),
          //   ),
          // ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            SingleChildScrollView(
              child: StreamBuilder<UserDataBG>(
                stream: DatabaseService(uid: user.uid).userBG,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    UserDataBG BG = snapshot.data;
                    return Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 8.0),
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                            child: Column(
                              children: [
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("Daily reminder",style: TextStyle(fontSize: 24),),
                                Icon(Icons.notifications,color: Colors.blue,size: 30,),
                  StreamBuilder<UserDataStatus>(
                  stream: DatabaseService(uid: user.uid).userStatus,
                  builder: (context, snapshot) {
                  if (snapshot.hasData)  {
                  UserDataStatus status = snapshot.data;
                  bool bmi = status.BMI;
                  bool bp = status.BP;
                  bool bg = status.BG;
                  print("BGnoti "+bg.toString());
                  if(bmi==true){
                  _showScheduleNotification();
                  _showScheduleNotification1();
                  }
                  return Switch(
                  value: status.BG,
                  onChanged: (value){
                  DatabaseService(uid: user.uid).updateNoti(bmi, bp, value);
                  setState(() {
                  });
                  },
                  activeTrackColor: Colors.lightGreenAccent,
                  activeColor: Colors.green,
                  );
                  }else{return Loading();}
                  }),],),
                                Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Column(children: [
                                    Text('Your Blood Glucose Result',
                                        style: TextStyle(fontSize: 28)),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        child: Icon(
                                          Icons.info_outline
                                        ),
                                        onTap: (){
                                          createAlertDialog(context);
                                        },
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Text(
                                      BG.bgTag.toString(),
                                      style: TextStyle(
                                          fontSize: 50,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(height: 40),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(BG.bg.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" mmol/L",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Text(BG.mealtime,
                                                  style:
                                                  TextStyle(fontSize: 25)),
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                    SizedBox(height: 25,),

                                    GestureDetector(
                                      onTap: (){
                                        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=> BGList()));
                                      },
                                      child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [Icon(Icons.history),
                                              Text(" Past Records",style: TextStyle(fontSize: 20),),
                                            ],
                                          )),
                                    ),
                                  ]),
                                ),
                              ],
                            ),
                          ),
                        ),
                        // Text("Past Records"),

                        // SingleChildScrollView(child: BMIList()),
                      ],
                    );
                  } else {
                    return Loading();
                  }
                },
              ),
            ),
            Container(child: BGchart()),
          ],
        ),
      ),
    );
  }
}



class BGchart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Color> colorOrange = <Color>[];
    colorOrange.add(Colors.deepOrange[50]);
    colorOrange.add(Colors.deepOrange[200]);
    colorOrange.add(Colors.deepOrange);

    final List<Color> colorBlue = <Color>[];
    colorBlue.add(Colors.lightBlueAccent[100]);
    colorBlue.add(Colors.lightBlueAccent[200]);
    colorBlue.add(Colors.blueAccent);

    final List<double> stopsOrange = <double>[];
    stopsOrange.add(0.0);
    stopsOrange.add(0.5);
    stopsOrange.add(1.0);

    final List<double> stopsBlue = <double>[];
    stopsBlue.add(0.0);
    stopsBlue.add(0.5);
    stopsBlue.add(1.0);

    final LinearGradient gradientColorsBlue =
    LinearGradient(colors: colorBlue, stops: stopsBlue);

    final user = Provider.of<User>(context);
    final CollectionReference fireData = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BG_List');
    return StreamBuilder(
      stream: fireData.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget widget;
        if (snapshot.hasData) {
          List<BGChartData> chartData1 = <BGChartData>[];
          for (int index = 0; index < snapshot.data.documents.length; index++) {
            DocumentSnapshot documentSnapshot = snapshot.data.documents[index];

            // here we are storing the data into a list which is used for chart’s data source
            chartData1.add(BGChartData.fromMap(documentSnapshot.data));
          }

          return Center(
            child: Padding(
                padding: const EdgeInsets.fromLTRB(15, 10, 15, 55),
                child: SfCartesianChart(
                  trackballBehavior: TrackballBehavior(
                    // Enables the trackball
                    enable: true,
                    tooltipSettings:
                    InteractiveTooltip(enable: true, color: Colors.blue[900],textStyle: TextStyle(fontSize:20 )),
                  ),
                  legend: Legend(
                      textStyle: TextStyle(fontSize: 22),
                      iconHeight: 25,
                      isVisible: true,
                      // Toogles the series visibility on tapping the legend item
                      toggleSeriesVisibility: true),
                  title: ChartTitle(
                      text: 'Blood Glocuse trend',
                      // Aligns the chart title to left
                      alignment: ChartAlignment.center,
                      textStyle: TextStyle(
                        color: Colors.blue[900],
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        fontSize: 25,
                      )),
                  zoomPanBehavior: ZoomPanBehavior(
                    enableDoubleTapZooming: true,
                    enablePinching: true,
                    enablePanning: true,
                    enableMouseWheelZooming: true,
                    enableSelectionZooming: false,
                    zoomMode: ZoomMode.x,
                  ),
                  primaryXAxis: DateTimeAxis(
                      title: AxisTitle(
                          text: 'Timestamp',
                          textStyle: TextStyle(
                            color: Colors.blueAccent[900],
                            fontFamily: 'Roboto',
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ))),
                  series: <ChartSeries<BGChartData, dynamic>>[
                    LineSeries<BGChartData, dynamic>(
                        name: 'Blood Glucose, mmol/L',
                        enableTooltip: true,
                        dataSource: chartData1,
                        xValueMapper: (BGChartData data, _) => data.xValue,
                        yValueMapper: (BGChartData data, _) => data.yValue,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        )),

                  ],
                )),
          );
        }
        return widget;
      },
    );
  }
}

class BGChartData {
  BGChartData({this.xValue, this.yValue,});

  BGChartData.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['dateTime'],
        yValue = dataMap['bg'];


  final Timestamp xValue;
  final num yValue;

}

class BG {
  BG({this.dateTime, this.bg});

  final Timestamp dateTime;
  final num bg;

}
