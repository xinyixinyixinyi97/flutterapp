import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutterapp/Pages/MedicalReport/editBPform.dart';
import 'package:flutterapp/shared/loading.dart';
import 'package:flutterapp/services/database.dart';
import 'package:flutterapp/models/user.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import 'BPList.dart';

class viewBPTrend extends StatefulWidget {
  @override
  _viewBPTrendState createState() => _viewBPTrendState();
}

class _viewBPTrendState extends State<viewBPTrend> {
  FlutterLocalNotificationsPlugin localNotification;
  @override
  void initState(){
    super.initState();
    var androidInitialize = new AndroidInitializationSettings('app_icon');
    var iOSInitialize = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(android: androidInitialize,iOS:iOSInitialize );
    localNotification = new FlutterLocalNotificationsPlugin();
    localNotification.initialize(initializationSettings);
  }

  Future _showNotification()async{
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    await localNotification.show(0,"Health Records Reminder", "Have you take your Blood Pressure record today?", generalNotificationDetails);

  }
  Future _showScheduleNotification()async{
    var time = Time(10,30,0);
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    localNotification..showDailyAtTime(0,"Daily Records Reminder", "Have you take your Blood Pressure record today?",time ,generalNotificationDetails);
  }
  Future _showScheduleNotification1()async{
    var time = Time(18,30,0);
    var androidDetails = new AndroidNotificationDetails("channelId", "Local Notification", "This is the description",importance: Importance.high);
    var iosDetails = new IOSNotificationDetails();
    var generalNotificationDetails = new NotificationDetails(android: androidDetails,iOS: iosDetails);
    localNotification..showDailyAtTime(0,"Daily Records Reminder", "Have you take your Blood Pressure record today?",time ,generalNotificationDetails);
  }
  createAlertDialog(BuildContext context) {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text("Blood Pressure Tips"),
        content: SingleChildScrollView(
          child: Container(
            height: 800,
            width: 500,
            child: ListView(children: <Widget>[
              Center(
                  child: Text(
                    'Hypertension / High Blood Pressure',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  )),
              SizedBox(height: 10,),
              Text('Your heart works like a pump. It contracts and relaxes. The Blood Pressure measurements are in two readings e.g. 130/85 mmHg. ',style: TextStyle(fontSize: 18),),
              Text('- Systolic pressure – the higher number (130 in this example) ',style: TextStyle(fontSize: 18),),
              Text('- Diastolic blood pressure – the lower number (85 in this example) ',style: TextStyle(fontSize: 18),),
              SizedBox(height: 10,),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: DataTable(
                    columns: [
                      DataColumn(label: Text(
                          'Category',
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                      )),
                      DataColumn(label: Text(
                          'Sys / Dia(mmHg)',
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
                      )),
                    ],
                    rows: [
                      DataRow(cells: [
                        DataCell(Text('Normal',style: TextStyle(fontSize: 18),)),
                        DataCell(Text('< 120 / < 80',style: TextStyle(fontSize: 18),)),
                      ]),
                      DataRow(cells: [
                        DataCell(Text('Pre-hypertension',style: TextStyle(fontSize: 18),)),
                        DataCell(Text('120-139 / 80-89',style: TextStyle(fontSize: 18),)),

                      ]),
                      DataRow(cells: [
                        DataCell(Text('Hypertension',style: TextStyle(fontSize: 18),)),
                        DataCell(Text('> 139 / > 89',style: TextStyle(fontSize: 18),)),

                      ]),
                      DataRow(cells: [
                        DataCell(Text('Isolated Systolic Hypertension',style: TextStyle(fontSize: 18),)),
                        DataCell(Text('> 139 / < 90',style: TextStyle(fontSize: 18),)),

                      ]),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Text("source : http://pendidikanpesakit.myhealth.gov.my",style: TextStyle(fontSize: 15,color: Colors.blueGrey),)

            ]),
          ),
        )  ,
        actions: <Widget>[MaterialButton(
          child: Text('Close'),
          onPressed: (){
            Navigator.pop(context);
          },
        ),
        ],
      );
    });
  }


  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    void _showEditBPPanel() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return Container(
              padding: EdgeInsets.fromLTRB(40,60,40,5),
              child: EditBPForm(),
            );
          },
          isScrollControlled: true);
    }

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          centerTitle: true,
          title: Text('Blood Pressure',
              style: TextStyle(
                fontSize: 25,
              )),
          backgroundColor: Colors.lightBlue[900],
          bottom: TabBar(
            tabs: [
              Tab(
                  icon: Icon(Icons.article_outlined),
                  child: Text(
                    "Track",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
              Tab(
                  icon: Icon(Icons.stacked_line_chart),
                  child: Text(
                    "Trends",
                    style: TextStyle(
                      fontSize: 25,
                    ),
                  )),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => _showEditBPPanel(),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20)),
          label: Text('Record manually', style: TextStyle(fontSize: 22),),
          // child: Container(
          //   constraints: BoxConstraints(
          //       maxWidth: 300.0, minHeight: 50.0),
          //   alignment: Alignment.center,
          //   child: Text(
          //     "Record manually",
          //     textAlign: TextAlign.center,
          //     style: TextStyle(
          //         fontSize: 22, color: Colors.white),
          //   ),
          // ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            StreamBuilder<UserDataBP>(
              stream: DatabaseService(uid: user.uid).userBP,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  UserDataBP BP = snapshot.data;
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                          child: Column(
                            children: [
                              Row(mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("Daily reminder",style: TextStyle(fontSize: 24),),
                                  Icon(Icons.notifications,color: Colors.blue,size: 30,),
                              StreamBuilder<UserDataStatus>(
                                  stream: DatabaseService(uid: user.uid).userStatus,
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData)  {
                                      UserDataStatus status = snapshot.data;
                                      bool bmi = status.BMI;
                                      bool bp = status.BP;
                                      bool bg = status.BG;
                                      print("BPnoti "+bp.toString());
                                      if(bmi==true){
                                        _showScheduleNotification();
                                        _showScheduleNotification1();
                                      }
                                      return Switch(
                                        value: status.BP,
                                        onChanged: (value){
                                          DatabaseService(uid: user.uid).updateNoti(bmi, value, bg);
                                          setState(() {
                                          });
                                        },
                                        activeTrackColor: Colors.lightGreenAccent,
                                        activeColor: Colors.green,
                                      );
                                    }else{return Loading();}
                                  }),],),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(18.0),
                                  child: Column(children: [
                                    Text('Your Blood Pressure Result',
                                        style: TextStyle(fontSize: 28)),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: GestureDetector(
                                        child: Icon(
                                            Icons.info_outline
                                        ),
                                        onTap: () {
                                          createAlertDialog(context);
                                        },
                                      ),
                                    ),
                                    Text(
                                      BP.hbpTag.toString(),
                                      style: TextStyle(
                                          fontSize: 50,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    SizedBox(height: 40),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(
                                                  BP.sys.toString() +
                                                      " / " +
                                                      BP.dia.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(' mmHg',
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                        SizedBox(width: 20),
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Row(
                                            children: [
                                              Text(BP.pul.toString(),
                                                  style:
                                                  TextStyle(fontSize: 30)),
                                              Text(" bpm",
                                                  style:
                                                  TextStyle(fontSize: 20))
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 25,),

                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(context,
                                            MaterialPageRoute(builder: (
                                                BuildContext context) =>
                                                BPList()));
                                      },
                                      child: Container(
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment
                                                .center,
                                            children: [Icon(Icons.history),
                                              Text(" Past Records",
                                                style: TextStyle(
                                                    fontSize: 20),),
                                            ],
                                          )),
                                    ),

                                  ]),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Loading();
                }
              },
            ),
            Container(child: BPchart()),
          ],
        ),
      ),
    );
  }
}

class BPchart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    final CollectionReference fireData = Firestore.instance
        .collection('UserData')
        .document(user.uid)
        .collection('med_report')
        .document('UserRecords')
        .collection('BP_List');
    return StreamBuilder(
      stream: fireData.snapshots(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget widget;
        if (snapshot.hasData) {
          List<ChartData> chartData = <ChartData>[];
          for (int index = 0; index < snapshot.data.documents.length; index++) {
            DocumentSnapshot documentSnapshot = snapshot.data.documents[index];

            // here we are storing the data into a list which is used for chart’s data source
            chartData.add(ChartData.fromMap(documentSnapshot.data));
          }
          return Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 10, 15, 75),
                child: SfCartesianChart(
                  trackballBehavior: TrackballBehavior(
                    // Enables the trackball
                    enable: true,
                    tooltipSettings: InteractiveTooltip(
                        enable: true, color: Colors.blue[900],textStyle: TextStyle(fontSize:20 )
                    ),),
                  legend: Legend(
                      isVisible: true,
                      toggleSeriesVisibility: true
                  ),
                  title: ChartTitle(
                      text: 'Blood Pressure trend',
                      // Aligns the chart title to left
                      alignment: ChartAlignment.center,
                      textStyle: TextStyle(
                        color: Colors.blueAccent,
                        fontFamily: 'Roboto',
                        fontWeight: FontWeight.w700,
                        fontSize: 20,
                      )),
                  zoomPanBehavior: ZoomPanBehavior(
                      enableDoubleTapZooming: true,
                      enablePinching: true,
                      enablePanning: true,
                      enableMouseWheelZooming: true,
                      enableSelectionZooming: false,
                      zoomMode: ZoomMode.x,
                      maximumZoomLevel: 2
                  ),
                  primaryXAxis: DateTimeAxis(
                    plotBands: <PlotBand>[
                      PlotBand(
                          verticalTextPadding: '5%',
                          horizontalTextPadding: '5%',
                          textAngle: 0,
                          text: 'Average',
                          start: 100,
                          end: 120,
                          textStyle:
                          TextStyle(color: Colors.deepOrange, fontSize: 16),
                          borderColor: Colors.red,
                          borderWidth: 2)
                    ],
                    // interval: 1,
                    // intervalType: DateTimeIntervalType.auto,
                  ),
                  series: <ChartSeries<ChartData, dynamic>>[
                    LineSeries<ChartData, dynamic>(
                        name: 'DIA (mmHg)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.yValue),
                    LineSeries<ChartData, dynamic>(
                        name: 'SYS (mmHg)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.y2Value),
                    LineSeries<ChartData, dynamic>(
                        name: 'PUL (bpm)',
                        dataSource: chartData,
                        markerSettings: MarkerSettings(
                          isVisible: true,
                        ),
                        xValueMapper: (ChartData data, _) => data.xValue,
                        yValueMapper: (ChartData data, _) => data.y3Value),
                  ],
                ),
              ));
        }
        return widget;
      },
    );
  }
}

class ChartData {
  ChartData({this.xValue, this.yValue, this.y2Value, this.y3Value});

  ChartData.fromMap(Map<String, dynamic> dataMap)
      : xValue = dataMap['dateTime'],
        yValue = dataMap['dia'],
        y2Value = dataMap['sys'],
        y3Value = dataMap['pul'];
  final Timestamp xValue;
  final int yValue;
  final int y2Value;
  final int y3Value;
}

